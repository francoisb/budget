desc "Deploys the application to Heroku"
task :deploy do
  sh "sqitch deploy --target production"
  sh "git push heroku master"
end
