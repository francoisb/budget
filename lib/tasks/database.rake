namespace :db do
  namespace :test do
    desc "Prepares the test database for running tests"
    task :prepare do
      sh "sudo -u postgres dropdb budget_test || exit 0"
      sh "sudo -u postgres createdb --owner $USER --encoding UTF-8 --locale en_US.UTF-8 --template template0 budget_test"
      sh "sqitch deploy --target test"
    end
  end

  desc "Drops and creates the development database"
  task :reset do
    sh "sudo -u postgres dropdb $USER || exit 0"
    sh "sudo -u postgres createdb --owner $USER --encoding UTF-8 --locale en_US.UTF-8 --template template0 $USER"
    sh "sqitch deploy --target development"
  end

  desc "Applies any pending migrations"
  task :migrate do
    sh "sqitch deploy development"
  end

  desc "Creates and downloads the latest production database backup"
  task :download do
    sh "heroku pg:backups capture --wait-interval=5 DATABASE_URL"
    backup = `heroku pg:backups | grep DATABASE | head -n 1 | awk '{print $1}'`.chomp
    sh "wget -q -O db/production.#{Time.now.utc.strftime("%Y-%m-%dT%H:%M:%S")}.pgdump $( heroku pg:backups public-url --quiet #{backup} )"
  end

  task :restore do
    most_recent_backup = FileList["db/production.*.pgdump"].sort.last
    sh "( dropdb vagrant || exit 0 ) && createdb vagrant -E UTF-8 -l en_US.UTF-8 -O vagrant -T template0"
    sh "pg_restore --no-owner --no-privileges -j2 -d vagrant #{most_recent_backup}"
  end

  namespace :migrate do
    desc "Reverts and applies the latest transaction"
    task :redo do
      sh "sqitch rebase -y @HEAD^"
    end
  end
end
