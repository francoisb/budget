require 'rake'
require 'rake/testtask'

Rake::TestTask.new(:test) do |t|
  t.pattern = "spec/**/*_spec.rb"
  t.libs    << "spec"
end


task :test do
  sh "sqitch deploy test"
  sh "psql budget_test -c 'create extension if not exists pgtap'"
  sh "sudo -u postgres pg_prove --ext sql --recurse --shuffle --dbname budget_test tests/database"
end
