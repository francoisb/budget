-- Revert budget:reconciliations from pg

SET client_min_messages TO 'warning';

BEGIN;

  DROP TABLE reconciliations CASCADE;

COMMIT;

-- vim: expandtab shiftwidth=2
