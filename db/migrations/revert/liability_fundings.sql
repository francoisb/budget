-- Revert budget:liability_fundings from pg

SET client_min_messages TO 'warning';

BEGIN;

  DROP TABLE liability_funding_entries CASCADE;
  DROP TABLE liability_fundings CASCADE;

COMMIT;

-- vim: expandtab shiftwidth=2
