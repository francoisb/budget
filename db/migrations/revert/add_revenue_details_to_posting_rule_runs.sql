-- Revert budget:add_revenue_details_to_posting_rule_runs from pg

SET client_min_messages TO 'warning';

BEGIN;

  ALTER TABLE posting_rule_runs
      ADD COLUMN run_on date not null
    , DROP COLUMN amount
    , DROP COLUMN account;

COMMIT;

-- vim: expandtab shiftwidth=2
