-- Revert budget:memo_accounts from pg

SET client_min_messages TO 'warning';

BEGIN;

  DROP TABLE memo_accounts CASCADE;

COMMIT;

-- vim: expandtab shiftwidth=2
