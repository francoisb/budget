-- Revert budget:transaction_entries from pg

SET client_min_messages TO 'warning';

BEGIN;

  DROP TABLE transaction_entries CASCADE;

COMMIT;

-- vim: expandtab shiftwidth=2
