-- Deploy budget:check_transaction_memos_validity to pg
-- requires: transaction_memos

SET client_min_messages TO 'warning';

BEGIN;

  CREATE OR REPLACE FUNCTION check_transaction_memos_validity() RETURNS trigger AS $$
  DECLARE
    rec RECORD;
  BEGIN
    FOR rec IN
      SELECT *
      FROM (
          SELECT
              transaction_id
            , account
            , sum(transaction_memos.amount_dt) memo_amount_dt
            , sum(transaction_memos.amount_ct) memo_amount_ct
          FROM transaction_memos
          GROUP BY transaction_id, account) t0
        JOIN (
          SELECT
              transaction_id
            , account
            , sum(transaction_entries.amount_dt) entry_amount_dt
            , sum(transaction_entries.amount_ct) entry_amount_ct
          FROM transaction_entries
          GROUP BY transaction_id, account) t1 USING (transaction_id, account)
      WHERE (memo_amount_dt - memo_amount_ct) <> (entry_amount_dt - entry_amount_ct)
    LOOP
      RAISE 'Transaction % is unbalanced in account %: memo''s sum is (% - %) = % while entry''s sum is (% - %) = %'
        , rec.transaction_id
        , rec.account
        , rec.memo_amount_dt, rec.memo_amount_ct, rec.memo_amount_dt - rec.memo_amount_ct
        , rec.entry_amount_dt, rec.entry_amount_ct, rec.entry_amount_dt - rec.entry_amount_ct;
    END LOOP;

    RETURN null;
  END;
  $$ LANGUAGE plpgsql;

  DROP TRIGGER IF EXISTS validate_transaction_memos ON transaction_memos;
  CREATE TRIGGER validate_transaction_memos
  AFTER INSERT OR UPDATE OR DELETE
  ON transaction_memos
  FOR EACH STATEMENT EXECUTE PROCEDURE check_transaction_memos_validity();

  CREATE OR REPLACE FUNCTION check_transaction_memos_account_is_asset() RETURNS trigger AS $$
  DECLARE
    rec RECORD;
  BEGIN
    FOR rec IN
      SELECT transaction_id, account, memo_account, account_kind
      FROM transaction_memos
        JOIN accounts USING (account)
      WHERE account_kind <> 'asset'
    LOOP
        RAISE EXCEPTION 'Transaction % is invalid: referenced account (%) from [%] memo account must be an [asset] account, not a [%]'
          , rec.transaction_id
          , rec.account
          , rec.memo_account
          , rec.account_kind;
    END LOOP;

    RETURN null;
  END;
  $$ LANGUAGE plpgsql;

  DROP TRIGGER IF EXISTS check_transaction_memos_account_is_asset ON transaction_memos;
  CREATE CONSTRAINT TRIGGER check_transaction_memos_account_is_asset
  AFTER INSERT OR UPDATE OF account
  ON transaction_memos
  FOR EACH ROW EXECUTE PROCEDURE check_transaction_memos_account_is_asset();

COMMIT;

-- vim: expandtab shiftwidth=2
