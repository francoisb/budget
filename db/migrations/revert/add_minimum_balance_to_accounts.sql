-- Revert budget:add_minimum_balance_to_accounts from pg

SET client_min_messages TO 'warning';

BEGIN;

  ALTER TABLE accounts DROP COLUMN minimum_balance;

COMMIT;

-- vim: expandtab shiftwidth=2
