-- Revert budget:accounts from pg

SET client_min_messages TO 'warning';

BEGIN;

  DROP TABLE accounts CASCADE;

COMMIT;

-- vim: expandtab shiftwidth=2
