-- Revert budget:add_booked_on_to_bank_transactions from pg

SET client_min_messages TO 'warning';

BEGIN;

  ALTER TABLE bank_transactions DROP COLUMN booked_on;

COMMIT;

-- vim: expandtab shiftwidth=2
