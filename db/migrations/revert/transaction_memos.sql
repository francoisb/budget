-- Revert budget:transaction_memos from pg

SET client_min_messages TO 'warning';

BEGIN;

  DROP TABLE transaction_memos CASCADE;

COMMIT;

-- vim: expandtab shiftwidth=2
