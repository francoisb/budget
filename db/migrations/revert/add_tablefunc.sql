-- Revert budget:add_tablefunc from pg

SET client_min_messages TO 'warning';

BEGIN;

  DROP EXTENSION "tablefunc";

COMMIT;

-- vim: expandtab shiftwidth=2
