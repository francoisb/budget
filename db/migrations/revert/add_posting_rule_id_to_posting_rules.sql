-- Revert budget:add_posting_rule_id_to_posting_rules from pg

SET client_min_messages TO 'warning';

BEGIN;

  ALTER TABLE posting_rules DROP COLUMN posting_rule_id;

COMMIT;

-- vim: expandtab shiftwidth=2
