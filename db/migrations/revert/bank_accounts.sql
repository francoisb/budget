-- Revert budget:bank_accounts from pg

SET client_min_messages TO 'warning';

BEGIN;

  DROP TABLE bank_accounts CASCADE;

COMMIT;

-- vim: expandtab shiftwidth=2
