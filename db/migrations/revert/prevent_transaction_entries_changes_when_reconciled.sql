-- Revert budget:prevent_transaction_entries_changes_when_reconciled from pg

SET client_min_messages TO 'warning';

BEGIN;
  ALTER TABLE transaction_entries
      DROP CONSTRAINT transaction_entries_pkey CASCADE
    , ADD PRIMARY KEY(transaction_id, account);

  ALTER TABLE reconciled_transactions ADD FOREIGN KEY(transaction_id, account) REFERENCES transaction_entries(transaction_id, account) ON UPDATE CASCADE ON DELETE CASCADE;
  ALTER TABLE transaction_memos ADD FOREIGN KEY(transaction_id, account) REFERENCES transaction_entries(transaction_id, account) ON UPDATE CASCADE ON DELETE CASCADE;
  ALTER TABLE reconciliation_entries
      DROP COLUMN amount_dt
    , DROP COLUMN amount_ct
    , ADD FOREIGN KEY(transaction_id, account) REFERENCES transaction_entries ON UPDATE RESTRICT ON DELETE RESTRICT;

COMMIT;

-- vim: expandtab shiftwidth=2
