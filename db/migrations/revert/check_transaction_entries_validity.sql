-- Revert budget:check_transaction_entries_validity from pg

SET client_min_messages TO 'warning';

BEGIN;

  DROP FUNCTION check_transaction_entries_validity() CASCADE;

COMMIT;

-- vim: expandtab shiftwidth=2
