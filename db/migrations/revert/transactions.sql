-- Revert budget:transactions from pg

SET client_min_messages TO 'warning';

BEGIN;

  DROP TABLE transactions CASCADE;

COMMIT;

-- vim: expandtab shiftwidth=2
