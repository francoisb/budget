-- Revert budget:reconciliation_entries from pg

SET client_min_messages TO 'warning';

BEGIN;

  DROP TABLE reconciliation_entries CASCADE;

COMMIT;

-- vim: expandtab shiftwidth=2
