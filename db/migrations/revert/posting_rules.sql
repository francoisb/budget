-- Revert budget:posting_rules from pg

SET client_min_messages TO 'warning';

BEGIN;

  DROP TABLE fixed_rate_posting_rule_entries CASCADE;
  DROP TABLE fixed_amount_posting_rule_entries CASCADE;
  DROP TABLE posting_rule_runs CASCADE;
  DROP TABLE posting_rules CASCADE;

COMMIT;

-- vim: expandtab shiftwidth=2
