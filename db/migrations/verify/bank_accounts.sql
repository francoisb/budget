-- Verify budget:bank_accounts on pg

SET client_min_messages TO 'warning';

BEGIN;

  SELECT account, account_number_hash, bank_account_id FROM bank_accounts WHERE false;

ROLLBACK;

-- vim: expandtab shiftwidth=2
