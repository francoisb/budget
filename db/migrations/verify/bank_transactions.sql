-- Verify budget:bank_transactions on pg

SET client_min_messages TO 'warning';

BEGIN;

  SELECT account_number_hash, posted_on, description, amount
  FROM bank_transactions WHERE false;

ROLLBACK;

-- vim: expandtab shiftwidth=2
