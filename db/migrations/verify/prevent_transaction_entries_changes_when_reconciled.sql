-- Verify budget:prevent_transaction_entries_changes_when_reconciled on pg

SET client_min_messages TO 'warning';

BEGIN;

  SELECT statement_posted_on, account, transaction_id, amount_dt, amount_ct, reconciled_on
  FROM reconciliation_entries
  WHERE false;

ROLLBACK;

-- vim: expandtab shiftwidth=2
