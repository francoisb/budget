-- Verify budget:liability_fundings on pg

SET client_min_messages TO 'warning';

BEGIN;

  SELECT account, funding_transaction_id
  FROM liability_fundings
  WHERE false;

  SELECT account, funding_transaction_id, transaction_id
  FROM liability_funding_entries
  WHERE false;

ROLLBACK;

-- vim: expandtab shiftwidth=2
