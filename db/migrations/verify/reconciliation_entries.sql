-- Verify budget:reconciliation_entries on pg

SET client_min_messages TO 'warning';

BEGIN;

  SELECT account, statement_posted_on, transaction_id
  FROM reconciliation_entries
  WHERE false;

ROLLBACK;

-- vim: expandtab shiftwidth=2
