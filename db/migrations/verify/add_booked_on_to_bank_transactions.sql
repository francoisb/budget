-- Verify budget:add_booked_on_to_bank_transactions on pg

SET client_min_messages TO 'warning';

BEGIN;

  SELECT booked_on FROM bank_transactions WHERE false;

ROLLBACK;

-- vim: expandtab shiftwidth=2
