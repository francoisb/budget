-- Verify budget:memo_accounts on pg

SET client_min_messages TO 'warning';

BEGIN;

  SELECT memo_account, memo_account_id FROM memo_accounts WHERE false;

ROLLBACK;

-- vim: expandtab shiftwidth=2
