-- Verify budget:record_reconciliations_against_transaction_entries on pg

SET client_min_messages TO 'warning';

BEGIN;

  SELECT account FROM reconciled_transactions WHERE false;

ROLLBACK;

-- vim: expandtab shiftwidth=2
