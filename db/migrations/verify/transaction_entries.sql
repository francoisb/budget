-- Verify budget:transaction_entries on pg

SET client_min_messages TO 'warning';

BEGIN;

  SELECT transaction_id, account, amount_dt, amount_ct, transaction_entry_id
  FROM transaction_entries
  WHERE false;

ROLLBACK;

-- vim: expandtab shiftwidth=2
