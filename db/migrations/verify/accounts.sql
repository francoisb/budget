-- Verify budget:accounts on pg

SET client_min_messages TO 'warning';

BEGIN;

  SELECT account, account_kind, account_id FROM accounts WHERE false;

ROLLBACK;

-- vim: expandtab shiftwidth=2
