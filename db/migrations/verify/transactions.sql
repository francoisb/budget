-- Verify budget:transactions on pg

SET client_min_messages TO 'warning';

BEGIN;

  SELECT transaction_id, posted_on, booked_on, description
  FROM transactions
  WHERE false;

ROLLBACK;

-- vim: expandtab shiftwidth=2
