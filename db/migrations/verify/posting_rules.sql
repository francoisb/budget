-- Verify budget:posting_rules on pg

SET client_min_messages TO 'warning';

BEGIN;

  SELECT posting_rule FROM posting_rules WHERE false;
  SELECT posting_rule, transaction_id, amount, account FROM posting_rule_runs WHERE false;

  SELECT posting_rule, memo_account, rate   FROM fixed_rate_posting_rule_entries WHERE false;
  SELECT posting_rule, memo_account, amount FROM fixed_amount_posting_rule_entries WHERE false;

ROLLBACK;

-- vim: expandtab shiftwidth=2
