-- Verify budget:reconciled_transactions on pg

SET client_min_messages TO 'warning';

BEGIN;

  SELECT transaction_id, bank_transaction_id, reconciled_on FROM reconciled_transactions WHERE false;

ROLLBACK;

-- vim: expandtab shiftwidth=2
