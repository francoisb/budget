-- Verify budget:add_posting_rule_id_to_posting_rules on pg

SET client_min_messages TO 'warning';

BEGIN;

  SELECT posting_rule, posting_rule_id
  FROM posting_rules
  WHERE false;

ROLLBACK;

-- vim: expandtab shiftwidth=2
