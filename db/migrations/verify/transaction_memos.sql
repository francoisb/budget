-- Verify budget:transaction_memos on pg

SET client_min_messages TO 'warning';

BEGIN;

  SELECT transaction_id, account, memo_account, amount_dt, amount_ct, transaction_memo_id
  FROM transaction_memos
  WHERE false;

ROLLBACK;

-- vim: expandtab shiftwidth=2
