-- Verify budget:add_revenue_details_to_posting_rule_runs on pg

SET client_min_messages TO 'warning';

BEGIN;

  SELECT posting_rule, transaction_id, amount, account
  FROM posting_rule_runs
  WHERE false;

ROLLBACK;

-- vim: expandtab shiftwidth=2
