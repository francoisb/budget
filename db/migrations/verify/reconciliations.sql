-- Verify budget:reconciliations on pg

SET client_min_messages TO 'warning';

BEGIN;

  SELECT account, booked_on, statement_posted_on, balance, reconciliation_id
  FROM reconciliations
  WHERE false;

ROLLBACK;

-- vim: expandtab shiftwidth=2
