-- Verify budget:add_minimum_balance_to_accounts on pg

SET client_min_messages TO 'warning';

BEGIN;

  SELECT minimum_balance
  FROM accounts
  WHERE false;

ROLLBACK;

-- vim: expandtab shiftwidth=2
