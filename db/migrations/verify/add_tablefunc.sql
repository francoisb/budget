-- Verify budget:add_tablefunc on pg

SET client_min_messages TO 'warning';

BEGIN;

  SELECT has_function_privilege('crosstab(text, text)', 'execute');

ROLLBACK;

-- vim: expandtab shiftwidth=2
