-- Verify budget:check_transaction_memos_validity on pg

SET client_min_messages TO 'warning';

BEGIN;

  SELECT has_function_privilege('check_transaction_memos_validity()', 'execute');

ROLLBACK;

-- vim: expandtab shiftwidth=2
