-- Deploy budget:accounts to pg

SET client_min_messages TO 'warning';

BEGIN;

  CREATE TABLE accounts(
      account text not null
    , account_kind text not null
    , account_id serial not null unique

    , constraint account_not_empty check(length(account) > 0)
    , constraint account_is_trimmed check(trim(account) = account)
    , constraint account_kind_in_list check(account_kind in ('asset', 'liability', 'revenue', 'expense', 'equity'))

    , primary key(account)
  );

  COMMENT ON TABLE accounts IS 'The chart of accounts for this accounting system';
  COMMENT ON COLUMN accounts.account IS 'Names this financial account, or ledger';
  COMMENT ON COLUMN accounts.account_kind IS 'Determines what type of account this is. Refer to https://en.wikipedia.org/wiki/Account_%28accountancy%29 for some details';

COMMIT;

-- vim: expandtab shiftwidth=2
