-- Deploy budget:posting_rules to pg
-- requires: accounts
-- requires: memo_accounts
-- requires: transactions

SET client_min_messages TO 'warning';

BEGIN;

  CREATE TABLE posting_rules(
      posting_rule text

    , primary key(posting_rule)
  );

  CREATE TABLE fixed_rate_posting_rule_entries(
      posting_rule text not null
    , memo_account text not null
    , rate decimal not null

    , primary key(posting_rule, memo_account)
    , foreign key(posting_rule) references posting_rules on delete cascade on update cascade
    , foreign key(memo_account) references memo_accounts on delete cascade on update cascade
  );

  CREATE TABLE fixed_amount_posting_rule_entries(
      posting_rule text not null
    , memo_account text not null
    , amount numeric not null

    , primary key(posting_rule, memo_account)
    , foreign key(posting_rule) references posting_rules on delete cascade on update cascade
    , foreign key(memo_account) references memo_accounts on delete cascade on update cascade
  );

  CREATE TABLE posting_rule_runs(
      posting_rule text not null
    , transaction_id uuid not null
    , run_on date not null

    , primary key(posting_rule, transaction_id)
    , foreign key(posting_rule) references posting_rules on delete cascade on update cascade
    , foreign key(transaction_id) references transactions on delete cascade on update cascade
  );

COMMIT;

-- vim: expandtab shiftwidth=2
