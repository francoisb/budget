-- Deploy budget:bank_accounts to pg

SET client_min_messages TO 'warning';

BEGIN;

  CREATE TABLE bank_accounts(
      account_number_hash text not null
    , account text not null
    , constraint account_number_hash_is_not_empty check(length(account_number_hash) > 0)
    , constraint account_number_hash_is_trimmed check(trim(account_number_hash) = account_number_hash)
    , bank_account_id serial not null unique

    , primary key(account_number_hash)
    , foreign key(account) references accounts on update cascade on delete cascade
  );

  COMMENT ON TABLE bank_accounts IS 'Enables us to know which accounts are bank accounts, as opposed to other types of accounts';
  COMMENT ON COLUMN bank_accounts.account_number_hash IS 'A SHA256 hash of the actual bank account number. This is to prevent data leaks, in case the database contents are stolen';
  COMMENT ON COLUMN bank_accounts.account IS 'References the internal account to which bank transactions belong to';

COMMIT;

-- vim: expandtab shiftwidth=2
