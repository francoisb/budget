-- Deploy budget:reconciliation_entries to pg
-- requires: reconciliations

SET client_min_messages TO 'warning';

BEGIN;

  CREATE TABLE reconciliation_entries(
      account text not null
    , statement_posted_on date
    , transaction_id uuid not null
    , reconciled_on date not null

    , primary key(account, statement_posted_on, transaction_id)
    , foreign key(account, statement_posted_on) references reconciliations on update cascade on delete cascade
    , foreign key(transaction_id, account) references transaction_entries on update cascade on delete cascade
  );

  COMMENT ON TABLE reconciliation_entries IS 'Records which transactions we have seen on which bank statements';
  COMMENT ON COLUMN reconciliation_entries.account IS 'References the bank account we''re reconciling';
  COMMENT ON COLUMN reconciliation_entries.statement_posted_on IS 'References the bank account''s statement''s date';
  COMMENT ON COLUMN reconciliation_entries.transaction_id IS 'References a transaction';

COMMIT;

-- vim: expandtab shiftwidth=2
