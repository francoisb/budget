-- Deploy budget:bank_transactions to pg
-- requires: bank_accounts

SET client_min_messages TO 'warning';

BEGIN;

  CREATE TABLE bank_transactions(
      bank_transaction_id uuid not null
    , account_number_hash text not null
    , posted_on date not null
    , description text not null
    , amount numeric not null

    , constraint description_is_trimmed check(trim(description) = description)

    , primary key(bank_transaction_id)
    , unique(account_number_hash, posted_on, description, amount)
    , foreign key(account_number_hash) references bank_accounts on update cascade on delete cascade
  );

  CREATE INDEX bank_transactions_by_account_and_posted_on ON bank_transactions(account_number_hash, posted_on, description);

  COMMENT ON TABLE bank_transactions IS 'Records actual bank transactions';
  COMMENT ON COLUMN bank_transactions.account_number_hash IS 'References the bank account which posted this transaction';
  COMMENT ON COLUMN bank_transactions.posted_on IS 'The date at which the transaction as recorded by the bank';
  COMMENT ON COLUMN bank_transactions.description IS 'The description as provided by the bank';
  COMMENT ON COLUMN bank_transactions.amount IS 'The amount as provided by the bank';
COMMIT;

-- vim: expandtab shiftwidth=2
