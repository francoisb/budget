-- Deploy budget:record_reconciliations_against_transaction_entries to pg
-- requires: reconciled_transactions

SET client_min_messages TO 'warning';

BEGIN;

  ALTER TABLE reconciled_transactions ADD COLUMN account text;

  UPDATE reconciled_transactions
  SET account = t0.account
  FROM (
    SELECT transaction_id, bank_transaction_id, bank_accounts.account
    FROM reconciled_transactions
      JOIN transaction_entries USING (transaction_id)
      JOIN bank_transactions USING (bank_transaction_id)
      JOIN bank_accounts USING (account_number_hash)
    WHERE bank_accounts.account = transaction_entries.account) t0
  WHERE reconciled_transactions.transaction_id = t0.transaction_id
    AND reconciled_transactions.bank_transaction_id = t0.bank_transaction_id;

  ALTER TABLE reconciled_transactions
      DROP CONSTRAINT reconciled_transactions_pkey
    , DROP CONSTRAINT reconciled_transactions_transaction_id_fkey
    , ADD PRIMARY KEY(bank_transaction_id, transaction_id, account)
    , ADD FOREIGN KEY(transaction_id, account) REFERENCES transaction_entries ON UPDATE cascade ON DELETE cascade;

COMMIT;

-- vim: expandtab shiftwidth=2
