-- Deploy budget:add_booked_on_to_bank_transactions to pg
-- requires: bank_transactions

SET client_min_messages TO 'warning';

BEGIN;

  ALTER TABLE bank_transactions ADD COLUMN booked_on date;
  UPDATE bank_transactions SET booked_on = posted_on;
  ALTER TABLE bank_transactions ALTER COLUMN booked_on SET NOT NULL;

COMMIT;

-- vim: expandtab shiftwidth=2
