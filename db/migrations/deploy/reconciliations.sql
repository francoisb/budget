-- Deploy budget:reconciliations to pg
-- requires: accounts
-- requires: transactions

SET client_min_messages TO 'warning';

BEGIN;

  CREATE TABLE reconciliations(
      account text not null
    , statement_posted_on date not null
    , balance numeric not null
    , booked_on date not null
    , reconciliation_id serial not null unique

    , primary key(account, statement_posted_on)
    , foreign key(account) references accounts
  );

  COMMENT ON TABLE reconciliations IS 'Records reconciliations between bank statements received in the "mail" and what the system thinks happened to an account.';
  COMMENT ON COLUMN reconciliations.account IS 'References the account which we''re reconciling.';
  COMMENT ON COLUMN reconciliations.statement_posted_on IS 'Records the bank statement''s date.';
  COMMENT ON COLUMN reconciliations.balance IS 'Records the bank statement''s balance.';
  COMMENT ON COLUMN reconciliations.booked_on IS 'Records the date on which we did data entry.';
  COMMENT ON COLUMN reconciliations.reconciliation_id IS 'Records a unique ID that identifies this specific row. Only used in the UI.';

COMMIT;

-- vim: expandtab shiftwidth=2
