-- Deploy budget:liability_fundings to pg
-- requires: transactions

SET client_min_messages TO 'warning';

BEGIN;

  CREATE TABLE liability_fundings(
      account text not null
    , funding_transaction_id uuid not null

    , primary key(account, funding_transaction_id)
    , foreign key(funding_transaction_id, account) references transaction_entries
  );

  COMMENT ON TABLE liability_fundings IS 'Records instances of reimbursements of liabilities';
  COMMENT ON COLUMN liability_fundings.account IS 'Which liability account are we funding';
  COMMENT ON COLUMN liability_fundings.funding_transaction_id IS 'Which transaction ID describes the funding process into the liability account';

  CREATE TABLE liability_funding_entries(
      account text not null
    , funding_transaction_id uuid not null
    , transaction_id uuid not null

    , primary key(account, funding_transaction_id, transaction_id)
    , foreign key(account, funding_transaction_id) references liability_fundings
  );

  COMMENT ON TABLE liability_funding_entries IS 'Records which liability transactions have been reimbursed';
  COMMENT ON COLUMN liability_funding_entries.account IS 'References liability_fundings';
  COMMENT ON COLUMN liability_funding_entries.funding_transaction_id IS 'References liability_fundings';
  COMMENT ON COLUMN liability_funding_entries.transaction_id IS 'References the liability transaction that is being reimbursed';

COMMIT;

-- vim: expandtab shiftwidth=2
