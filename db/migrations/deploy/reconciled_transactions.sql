-- Deploy budget:reconciled_transactions to pg
-- requires: bank_transactions
-- requires: transactions

SET client_min_messages TO 'warning';

BEGIN;

  CREATE TABLE reconciled_transactions(
      transaction_id uuid not null
    , bank_transaction_id uuid not null
    , reconciled_on date not null

    , primary key(transaction_id, bank_transaction_id)
    , foreign key(transaction_id) references transactions on update cascade on delete cascade
    , foreign key(bank_transaction_id) references bank_transactions on update cascade on delete cascade
  );

COMMIT;

-- vim: expandtab shiftwidth=2
