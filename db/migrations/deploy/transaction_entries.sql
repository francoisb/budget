-- Deploy budget:transaction_entries to pg
-- requires: transactions
-- requires: accounts

SET client_min_messages TO 'warning';

BEGIN;

  CREATE TABLE transaction_entries(
      transaction_id uuid not null
    , account text not null
    , amount_dt numeric not null
    , amount_ct numeric not null
    , transaction_entry_id serial not null unique

    , primary key(transaction_id, account)
    , foreign key(transaction_id) references transactions on update cascade on delete cascade
    , foreign key(account) references accounts on update cascade on delete cascade
  );

  COMMENT ON TABLE transaction_entries IS 'Records the list of accounts that are touched by each transaction';

  COMMENT ON COLUMN transaction_entries.transaction_id IS 'References the transaction this entry is a part of';
  COMMENT ON COLUMN transaction_entries.account        IS 'References the account that this entry refers to';
  COMMENT ON COLUMN transaction_entries.amount_dt      IS 'The debited amount from this account';
  COMMENT ON COLUMN transaction_entries.amount_ct      IS 'The credited amount from this account';

COMMIT;

-- vim: expandtab shiftwidth=2
