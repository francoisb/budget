-- Deploy budget:check_transaction_entries_validity to pg
-- requires: transaction_entries

SET client_min_messages TO 'warning';

BEGIN;

  CREATE OR REPLACE FUNCTION check_transaction_entries_validity() RETURNS trigger AS $$
  DECLARE
    rec RECORD;
  BEGIN
    FOR rec IN
      SELECT transaction_id, amount_dt, amount_ct
      FROM (
          SELECT transaction_id, sum(amount_dt) amount_dt, sum(amount_ct) amount_ct
          FROM transaction_entries
          GROUP BY transaction_id) t1
      WHERE amount_dt <> amount_ct
    LOOP
      RAISE 'Transaction % is unbalanced: debits are % while credits are %', rec.transaction_id, rec.amount_dt, rec.amount_ct;
    END LOOP;

    RETURN null;
  END;
  $$ LANGUAGE plpgsql;

  DROP TRIGGER IF EXISTS validate_transaction_entries ON transaction_entries;
  CREATE TRIGGER validate_transaction_entries
  AFTER INSERT OR UPDATE OR DELETE
  ON transaction_entries
  FOR EACH STATEMENT EXECUTE PROCEDURE check_transaction_entries_validity();

COMMIT;

-- vim: expandtab shiftwidth=2
