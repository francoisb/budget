-- Deploy budget:add_tablefunc to pg

SET client_min_messages TO 'warning';

BEGIN;

  CREATE EXTENSION "tablefunc";

COMMIT;

-- vim: expandtab shiftwidth=2
