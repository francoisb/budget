-- Deploy budget:check_transaction_memos_validity to pg
-- requires: transaction_memos

SET client_min_messages TO 'warning';

BEGIN;

  CREATE OR REPLACE FUNCTION check_transaction_memos_validity() RETURNS trigger AS $$
  DECLARE
    rec RECORD;
  BEGIN
    FOR rec IN
      SELECT *
      FROM (
          SELECT transaction_id, sum(amount) memo_amount
          FROM transaction_memos
          GROUP BY transaction_id) t1
        JOIN (
          SELECT transaction_id, sum(amount_dt) txn_amount
          FROM transaction_entries
          GROUP BY transaction_id) t2 USING (transaction_id)
      WHERE memo_amount <> txn_amount AND memo_amount <> 0
    LOOP
      RAISE 'Transaction ID % is unbalanced: transaction''s amount is % while memos are %', rec.transaction_id, rec.txn_amount, rec.memo_amount;
    END LOOP;

    RETURN null;
  END;
  $$ LANGUAGE plpgsql;

  DROP TRIGGER IF EXISTS validate_transaction_memos ON transaction_memos;
  CREATE TRIGGER validate_transaction_memos
  AFTER INSERT OR UPDATE OR DELETE
  ON transaction_memos
  FOR EACH STATEMENT EXECUTE PROCEDURE check_transaction_memos_validity();

COMMIT;

-- vim: expandtab shiftwidth=2
