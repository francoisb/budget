-- Deploy budget:transaction_memos to pg
-- requires: transactions
-- requires: memo_accounts

SET client_min_messages TO 'warning';

BEGIN;

  CREATE TABLE transaction_memos(
      transaction_id uuid not null
    , account text not null
    , memo_account text not null
    , amount_dt numeric not null
    , amount_ct numeric not null
    , transaction_memo_id serial not null unique

    , primary key(transaction_id, account, memo_account)
    , foreign key(transaction_id, account) references transaction_entries on update cascade on delete cascade
    , foreign key(memo_account) references memo_accounts on update cascade on delete cascade
  );

  COMMENT ON TABLE transaction_memos IS 'Records earmarked money (money that is set aside) without physically moving money between accounts';

  COMMENT ON COLUMN transaction_memos.transaction_id IS 'References the transaction that is the originator of this allocation';
  COMMENT ON COLUMN transaction_memos.account        IS 'References the account which funded this memo account. This must be an asset account.';
  COMMENT ON COLUMN transaction_memos.memo_account   IS 'References the envelope / memo account into or from which money is taken / moved from';
  COMMENT ON COLUMN transaction_memos.amount_dt      IS 'Records the amount of money set aside for this memo account.';
  COMMENT ON COLUMN transaction_memos.amount_ct      IS 'Records the amount of money we used from this memo account.';

  CREATE OR REPLACE FUNCTION check_transaction_memo_account_is_asset() RETURNS trigger AS $$
  BEGIN
    RETURN NEW;
  END;
  $$ LANGUAGE plpgsql;

COMMIT;

-- vim: expandtab shiftwidth=2
