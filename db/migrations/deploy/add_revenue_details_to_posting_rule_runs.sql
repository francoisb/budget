-- Deploy budget:add_revenue_details_to_posting_rule_runs to pg
-- requires: posting_rules

SET client_min_messages TO 'warning';

BEGIN;

  ALTER TABLE posting_rule_runs
      DROP COLUMN run_on
    , ADD COLUMN amount numeric not null check(amount > 0)
    , ADD COLUMN account text not null
    , ADD FOREIGN KEY(account) REFERENCES accounts;

COMMIT;

-- vim: expandtab shiftwidth=2
