-- Deploy budget:transactions to pg
-- requires: install_uuid

SET client_min_messages TO 'warning';

BEGIN;

  CREATE TABLE transactions(
      transaction_id uuid not null
    , posted_on date not null
    , booked_on date not null
    , description text

    , primary key(transaction_id)
  );

  COMMENT ON TABLE transactions IS 'Records the list of transactions that occur against accounts';
  COMMENT ON COLUMN transactions.transaction_id IS 'A surrogate primary key';
  COMMENT ON COLUMN transactions.posted_on IS 'The date on which the transaction occured';
  COMMENT ON COLUMN transactions.booked_on IS 'The date on which we recorded the transaction in the system';
  COMMENT ON COLUMN transactions.description IS 'A textual description of the transaction';

COMMIT;

-- vim: expandtab shiftwidth=2
