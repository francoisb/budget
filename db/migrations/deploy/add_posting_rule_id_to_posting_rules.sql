-- Deploy budget:add_posting_rule_id_to_posting_rules to pg
-- requires: posting_rules

SET client_min_messages TO 'warning';

BEGIN;

  ALTER TABLE posting_rules ADD COLUMN posting_rule_id serial not null unique;

COMMIT;

-- vim: expandtab shiftwidth=2
