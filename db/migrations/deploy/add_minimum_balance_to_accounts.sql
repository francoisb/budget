-- Deploy budget:add_minimum_balance_to_accounts to pg
-- requires: accounts

SET client_min_messages TO 'warning';

BEGIN;

  ALTER TABLE accounts ADD COLUMN minimum_balance NUMERIC, ADD CONSTRAINT minimum_balance_is_positive CHECK(minimum_balance IS NULL OR minimum_balance >= 0);
  COMMENT ON COLUMN accounts.minimum_balance IS 'Indicates to the system a minimum balance we always want to keep in the account. Usually, the account would be an asset account.';

COMMIT;

-- vim: expandtab shiftwidth=2
