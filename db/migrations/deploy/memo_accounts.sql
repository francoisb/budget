-- Deploy budget:memo_accounts to pg

SET client_min_messages TO 'warning';

BEGIN;

  CREATE TABLE memo_accounts(
      memo_account text not null
    , memo_account_id serial not null unique

    , constraint memo_account_not_empty check(length(memo_account) > 0)
    , constraint memo_account_is_trimmed check(trim(memo_account) = memo_account)

    , primary key(memo_account)
  );

  COMMENT ON TABLE memo_accounts IS 'The list of virtual accounts that exist in this accounting system. Virtual accounts exist to remember how much money is earmarked for projects in an asset account.';
  COMMENT ON COLUMN memo_accounts.memo_account IS 'Names this virtual account';

COMMIT;

-- vim: expandtab shiftwidth=2
