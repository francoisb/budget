-- Deploy budget:prevent_transaction_entries_changes_when_reconciled to pg
-- requires: reconciliation_entries
-- requires: transaction_entries

SET client_min_messages TO 'warning';

BEGIN;

  -- Change transaction_entries' primary key: we have 3 foreign key constraints which have to change
  ALTER TABLE transaction_entries
      DROP CONSTRAINT transaction_entries_pkey CASCADE
    , ADD PRIMARY KEY(transaction_id, account, amount_dt, amount_ct)
    , ADD UNIQUE(transaction_id, account);

  -- Two that we recreate here immediately, and...
  ALTER TABLE reconciled_transactions ADD FOREIGN KEY(transaction_id, account) REFERENCES transaction_entries(transaction_id, account) ON UPDATE CASCADE ON DELETE CASCADE;
  ALTER TABLE transaction_memos ADD FOREIGN KEY(transaction_id, account) REFERENCES transaction_entries(transaction_id, account) ON UPDATE CASCADE ON DELETE CASCADE;

  -- One that we change to prevent any changes to reconciliations
  ALTER TABLE reconciliation_entries ADD COLUMN amount_dt numeric, ADD COLUMN amount_ct numeric;
  UPDATE reconciliation_entries
  SET amount_dt = te.amount_dt, amount_ct = te.amount_ct
  FROM transaction_entries te
  WHERE te.transaction_id = reconciliation_entries.transaction_id
    AND te.account = reconciliation_entries.account;
  ALTER TABLE reconciliation_entries
      ALTER COLUMN amount_dt SET NOT NULL
    , ALTER COLUMN amount_ct SET NOT NULL
    , ADD FOREIGN KEY(transaction_id, account, amount_dt, amount_ct) REFERENCES transaction_entries(transaction_id, account, amount_dt, amount_ct) ON UPDATE RESTRICT ON DELETE RESTRICT;

COMMIT;

-- vim: expandtab shiftwidth=2
