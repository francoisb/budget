require File.expand_path('../boot', __FILE__)

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "sprockets/railtie"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Budget
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    config.logger = Logger.new(STDERR)
    config.logger.formatter = lambda {|severity, datetime, progname, msg|
        "[#{"%-5s" % severity}] #{File.basename($0)}:#{Process.pid} - #{msg}\n"
    }

    config.autoload_paths << Rails.root + "app/forms"

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    config.i18n.default_locale = "fr-CA"
    config.i18n.available_locales = ["fr-CA", "en-CA"]

    # Enable Sequel probing
    config.skylight.probes += %w(sequel)
  end
end
