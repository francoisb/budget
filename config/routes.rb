Rails.application.routes.draw do
  root to: "memo_accounts#index"

  resources :accounts, only: %w(index create show edit update)
  resources :transactions, only: %w(index new create edit update destroy) do
    collection do
      get :graph
    end
  end
  post "/transactions/:bank_transaction_id", to: "transactions#create", as: :create_transaction_from_bank_transaction
  post "/transactions/:bank_transaction_id/reconcile", to: "transactions#create_with_reconciliation", as: :reconcile_bank_transaction

  resources :bank_transactions, only: :index do
    member do
      get "/new-transaction", to: "transactions#new_from_bank_transaction", as: :new_transaction_from
    end
  end
  get "/bank_transactions/import",  to: "bank_transactions#new_import", as: :new_bank_transactions_import
  post "/bank_transactions/import", to: "bank_transactions#import", as: :bank_transactions_import

  resources :memo_accounts, only: %w(index show create)
  resources :reconciliations, except: %w(new edit)
  resources :liability_fundings, only: %w(index show new create)
  resources :revenues, only: %w(new create)
  resources :posting_rules
end
