Sequel.extension :pg_array_ops
Sequel.extension :core_extensions

DB = Sequel.connect(ENV["DATABASE_URL"] || "postgresql:///")
DB.extension :pagination
DB.extension :pg_array

Sequel::Model.plugin :active_model
Sequel::Model.plugin :delay_add_association
Sequel::Model.plugin :string_stripper
Sequel::Model.plugin :update_primary_key

Sequel::Model.unrestrict_primary_key
