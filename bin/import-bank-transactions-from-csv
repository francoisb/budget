#!/usr/bin/env ruby
require_relative "../config/environment"

require "optparse"

AVAILABLE_FORMATS = %w(rbc desjardins).freeze
options = {}

OptionParser.new do |o|
  o.on("--format=FORMAT", "Indicates which data format to use, one of #{AVAILABLE_FORMATS.inspect}") do |format|
    options[:format] = format
  end
end.parse!

ARGV.each do |filename|
  io = nil

  begin
    io = if filename == "-" then
           STDIN
         else
           File.open(filename)
         end

    Rails.logger.info "Loading #{filename.inspect}"
    parser = BankTransactionParser.parser_class_from_format(options.fetch(:format)).new(io)
    parser.parse
    Rails.logger.info "Imported #{parser.line} transactions"
  ensure
    io.close if io
  end
end
