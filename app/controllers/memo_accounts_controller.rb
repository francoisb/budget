class MemoAccountsController < ApplicationController
  def index
    @memo_accounts_by_memo_account = MemoAccount.by_memo_account.to_a
    @memo_accounts_by_account = MemoAccount.by_account.to_a
    @accounts = Hash[*@memo_accounts_by_account.map{|row| [row.fetch(:account), row.fetch(:account_id)]}.uniq.flatten]
    @memo_accounts = Hash[*@memo_accounts_by_memo_account.map{|row| [row.fetch(:memo_account), row.fetch(:memo_account_id)]}.uniq.flatten]
  end

  def show
    @memo_account = MemoAccount[memo_account_id: params[:id]]
    raise "not found" unless memo_account

    @vm = MemoAccountTransactionsReport.new(params.fetch(:memo_account_transactions_report, {}).merge(memo_account: memo_account.memo_account))

    fixed_amount_posting_rule_entries_ds =
      FixedAmountPostingRuleEntry.
      join(PostingRule, [:posting_rule]).
      filter(memo_account: memo_account.memo_account).
      order{ lower(posting_rule) }.
      select{ [posting_rule_id, posting_rule, amount.as(:number)] }.select_append("amount".as(:rule))

    fixed_rate_posting_rule_entries_ds =
      FixedRatePostingRuleEntry.
      join(PostingRule, [:posting_rule]).
      filter(memo_account: memo_account.memo_account).
      order{ lower(posting_rule) }.
      select{ [posting_rule_id, posting_rule, rate.as(:number)] }.select_append("percentage".as(:rule))

    @posting_rule_entries = fixed_amount_posting_rule_entries_ds.to_a + fixed_rate_posting_rule_entries_ds.to_a
  end

  attr_reader :memo_account
  helper_method :memo_account
  private :memo_account
end
