require "stringio"

class BankTransactionsController < ApplicationController
  def index
    @vm = BankTransactionsReport.new(params[:bank_transactions_report]) do |scope|
      scope.paginate(params[:page].blank? ? 1 : Integer(params[:page]), 100)
    end
  end

  def new_import
    @vm = ImportForm.new(OpenStruct.new)
  end

  def import
    @vm = ImportForm.new(OpenStruct.new)
    if vm.validate(params[:bank_transactions_controller_import]) then
      raw_data = vm.data.read
      utf8_data = raw_data.encode("UTF-8", "Windows-1252", universal_newline: true)

      parser = BankTransactionParser.parser_class_from_format(vm.data_format).new(StringIO.new(utf8_data))
      num_imported_transactions = parser.parse
      redirect_to bank_transactions_url(:"bank_transactions_report[reconciled]" => "NO"), notice: t("bank_transactions.import.successful", count: num_imported_transactions)
    else
      render action: :new_import
    end
  rescue BankTransactionParser::BankAccountNotFound, BankTransactionParser::UnrecognizedBankTransactionFormatError, BankTransactionParser::TransactionAlreadyExists => e
    flash[:error] = t("bank_transactions.import.failed", message: e.message)
    render action: :new_import
  end

  class ImportForm < Reform::Form
    AVAILABLE_FORMATS = %w(desjardins rbc)

    def available_formats
      AVAILABLE_FORMATS
    end

    property :data_format, validates: {presence: true, inclusion: {within: AVAILABLE_FORMATS}}
    property :data, validates: {presence: true}
  end
end
