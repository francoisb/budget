class TransactionsController < ApplicationController
  def index
    @vm = TransactionsReport.new({order: :posted_on}.merge(params[:transactions_report] || {}))
  end

  def graph
    graph_transactions_report = {
      posted_on: [30.days.ago.at_beginning_of_month, Date.today.at_beginning_of_month - 1].map(&:to_s),
    }

    @vm = GraphTransactionsReport.new(graph_transactions_report.merge(params[:graph_transactions_report] || {}))
  end

  def new
    @vm = TransactionForm.new(
      Transaction.new(
        transaction_id: SecureRandom.uuid,
        booked_on: Date.today,
        posted_on: Date.today,
        description: "",
        memos: TRANSACTION_MEMOS_AVAILABLE.times.map { TransactionMemo.new },
        entries: TRANSACTION_ENTRIES_AVAILBLE.times.map{ TransactionEntry.new }))
  end

  def create
    if params.has_key?(:liability_funding) then
      @liability_funding = LiabilityFundingForm.new(LiabilityFunding.new)
      raise "invalid liability_funding" unless @liability_funding.validate(params[:liability_funding])
    end

    @vm = TransactionForm.new(
      Transaction.new(
        transaction_id: SecureRandom.uuid,
        booked_on: Date.today,
        posted_on: Date.today,
        description: "",
        memos: TRANSACTION_MEMOS_AVAILABLE.times.map { TransactionMemo.new },
        entries: TRANSACTION_ENTRIES_AVAILBLE.times.map{ TransactionEntry.new }))
    if vm.validate(params[:transaction]) then
      vm.save do |hash|
        DB.transaction do
          transaction = Transaction.create(transaction_id: vm.transaction_id, booked_on: vm.booked_on, posted_on: vm.posted_on, description: vm.description)
          raise transactions.errors.full_messages.inspect unless transaction.persisted?

          vm.entries.delete_if{|entry| entry.amount_dt.blank? && entry.amount_ct.blank?}
          vm.entries.each{|entry| entry.amount_dt = BigDecimal(entry.amount_dt)}
          vm.entries.each{|entry| entry.amount_ct = BigDecimal(entry.amount_ct)}
          entries = vm.entries.map do |entry|
            [transaction.transaction_id, entry.account, entry.amount_dt.blank? ? BigDecimal(0) : entry.amount_dt, entry.amount_ct.blank? ? BigDecimal(0) : entry.amount_ct]
          end.reject{|row| row[2, 2].all?(&:zero?)}
          TransactionEntry.import([:transaction_id, :account, :amount_dt, :amount_ct], entries)
          if vm.bank_transaction_id.present? then
            ReconciledTransaction.create(
              transaction_id: transaction.transaction_id,
              bank_transaction_id: vm.bank_transaction_id,
              account: BankTransaction.naked.filter(bank_transaction_id: vm.bank_transaction_id).join(BankAccount, [:account_number_hash]).select_map(:account).first,
              reconciled_on: Date.today)
          end

          memos = vm.memos.reject{|entry| [entry.amount_dt, entry.amount_ct].all?(&:blank?)}.map do |memo|
            amount_dt = BigDecimal(memo.amount_dt)
            amount_ct = BigDecimal(memo.amount_ct)
            [transaction.transaction_id, memo.account, memo.memo_account, amount_dt, amount_ct]
          end
          TransactionMemo.import([:transaction_id, :account, :memo_account, :amount_dt, :amount_ct], memos)

          if @liability_funding then
            @liability_funding.model.funding_transaction_id = transaction.transaction_id
            @liability_funding.save
          end
        end
      end

      if @liability_funding then
        redirect_to liability_fundings_url, notice: "Reimbursement recorded successfully"
      elsif vm.bank_transaction_id.blank? then
        redirect_to transactions_url, notice: "Transaction recorded successfully"
      else
        redirect_to bank_transactions_url(
          :"bank_transactions_report[reconciled]" => "NO",
          :"bank_transactions_report[bank_account_id]" => BankTransaction.naked.filter(bank_transaction_id: vm.bank_transaction_id).join(BankAccount, [:account_number_hash]).select_map(:bank_account_id).first)
      end
    else
      render action: :new
    end
  rescue Sequel::UniqueConstraintViolation, Sequel::ForeignKeyConstraintViolation, Sequel::DatabaseError => e
    flash.now[:notice] = "#{e.class}: #{e.message}"
    render action: :new
  end

  def new_from_bank_transaction
    @bank_transaction = BankTransaction[bank_transaction_id: params[:id]]
    raise "not found" unless bank_transaction

    account = Account.join(BankAccount, [:account])[account_number_hash: bank_transaction.account_number_hash]
    raise "not found" unless account

    entry = TransactionEntry.new
    entry.account = account.account
    case account.kind
    when "asset", "revenue"
      if bank_transaction.amount < 0 then
        entry.amount_dt = 0
        entry.amount_ct = bank_transaction.amount.abs
      else
        entry.amount_dt = bank_transaction.amount.abs
        entry.amount_ct = 0
      end
    when "liability", "expense", "equity"
      if bank_transaction.amount < 0 then
        entry.amount_dt = bank_transaction.amount.abs
        entry.amount_ct = 0
      else
        entry.amount_dt = 0
        entry.amount_ct = bank_transaction.amount.abs
      end
    else
      raise "ASSERTION ERROR: Unrecognized account kind for #{account.inspect}"
    end

    @vm = TransactionForm.new(
      Transaction.new(
        booked_on: Date.today,
        posted_on: bank_transaction.posted_on,
        description: bank_transaction.description,
        memos: TRANSACTION_MEMOS_AVAILABLE.times.map { TransactionMemo.new },
        entries: TRANSACTION_ENTRIES_AVAILBLE.times.map{ TransactionEntry.new }.unshift(entry)))
    vm.account = account.account
    vm.bank_transaction_id = bank_transaction.bank_transaction_id

    @reconciliation_form = ProcessingForm.new(vm.model) if candidates(account.account).any?

    render action: :new
  end

  def create_with_reconciliation
    bank_transaction = BankTransaction[bank_transaction_id: params[:bank_transaction_id]]
    raise "not found" unless bank_transaction

    DB.transaction do
      model = ReconciledTransaction.create(
        bank_transaction_id: bank_transaction.bank_transaction_id,
        transaction_id: params[:transaction][:transaction_id],
        account: bank_transaction.account,
        reconciled_on: Date.today)
      raise "not saved" unless model.persisted?
    end

    redirect_to bank_transactions_url(
      :"bank_transactions_report[reconciled]" => "NO",
      :"bank_transactions_report[bank_account_id]" => BankTransaction.naked.filter(bank_transaction_id: bank_transaction.bank_transaction_id).join(BankAccount, [:account_number_hash]).select_map(:bank_account_id).first)
  end

  def edit
    transaction = Transaction[transaction_id: params[:id]]
    raise "not found" unless transaction

    @vm = TransactionForm.new(transaction)

    missing_entries =  3.times.map{ TransactionEntry.new }
    missing_memos   = TRANSACTION_MEMOS_AVAILABLE.times.map{ TransactionMemo.new  }
    vm.entries = vm.entries + missing_entries
    vm.memos   = vm.memos   + missing_memos
  end

  def update
    transaction = Transaction[transaction_id: params[:id]]
    raise "not found" unless transaction

    @vm = TransactionForm.new(transaction)
    if vm.validate(params[:transaction]) then
      vm.save do |_|
        DB.transaction do
          # Create a completely new transaction
          new_transaction = Transaction.create(transaction.values.merge(transaction_id: SecureRandom.uuid))
          transaction_id = new_transaction.transaction_id

          vm.entries.each{|entry| entry.amount_dt = BigDecimal(entry.amount_dt)}
          vm.entries.each{|entry| entry.amount_ct = BigDecimal(entry.amount_ct)}
          entries = vm.entries.reject{|entry| entry.account.blank? || (entry.amount_dt.zero? && entry.amount_ct.zero?)}.map do |entry|
            [transaction_id, entry.account, entry.amount_dt.blank? ? BigDecimal(0) : entry.amount_dt, entry.amount_ct.blank? ? BigDecimal(0) : entry.amount_ct]
          end
          TransactionEntry.import([:transaction_id, :account, :amount_dt, :amount_ct], entries)
          ReconciledTransaction.naked.filter(transaction_id: transaction.transaction_id).each do |reconciliation|
            # Recreate any reconciliation transactions, pointing to the new transaction
            if entries.any?{|entry| entry[1] == reconciliation.fetch(:account)} then
              ReconciledTransaction.create(reconciliation.merge(transaction_id: transaction_id))
            end
          end

          memos = vm.memos.map do |memo|
            [transaction_id, memo.account, memo.memo_account, memo.amount_dt.blank? ? BigDecimal(0) : BigDecimal(memo.amount_dt), memo.amount_ct.blank? ? BigDecimal(0) : BigDecimal(memo.amount_ct)]
          end.reject{|row| row[3, 2].all?(&:zero?)}
          TransactionMemo.import([:transaction_id, :account, :memo_account, :amount_dt, :amount_ct], memos)

          # Delete the existing transaction, including all associated models...
          transaction.delete

          # Only to recreate it, by renaming the new transaction!
          Transaction.
            filter(transaction_id: new_transaction.transaction_id).
            update(transaction_id: transaction.transaction_id)
        end
      end
      redirect_to transactions_url, notice: "Successfully saved transaction"
    else
      missing_entries = (TRANSACTION_ENTRIES_AVAILBLE - vm.entries.size).times.map{ TransactionEntry.new }
      missing_memos   = (TRANSACTION_MEMOS_AVAILABLE - vm.memos.size).times.map{ TransactionMemo.new  }
      vm.entries = vm.entries + missing_entries
      vm.memos   = vm.memos   + missing_memos
      render action: :edit
    end
  rescue Sequel::ForeignKeyConstraintViolation => e
    logger.info "#{e.class}: #{e.message}"
    flash.now[:error] =
      case e.message
      when /reconciliation_entries_transaction_id_fkey/
        match = e.message.match(/[(]([a-f0-9]{8}(?:-[a-f0-9]{4}){3}-[a-f0-9]{12}), ([^,]+), ([^,]+), ([^,]+)[)]/)
        transaction_id = match[1]
        account        = match[2]
        amount_dt      = match[3]
        amount_ct      = match[4]

        entries_ds = ReconciliationEntry.
          filter(transaction_id: transaction_id, account: account, amount_dt: amount_dt, amount_ct: amount_ct).
          select(:statement_posted_on)
        reconciliation = Reconciliation.filter(statement_posted_on: entries_ds).first
        flash.now[:error_link] = reconciliation_url(reconciliation.reconciliation_id, anchor: transaction_id)
        I18n.translate "transactions.update.reconciliation_entries_transaction_id_fkey_html",
          transaction_id: transaction_id,
          account: account,
          statement_posted_on: reconciliation.statement_posted_on
      else
        "#{e.class}: #{e.message}"
      end

    missing_entries = (TRANSACTION_ENTRIES_AVAILBLE - vm.entries.size).times.map{ TransactionEntry.new }
    missing_memos   = (TRANSACTION_MEMOS_AVAILABLE - vm.memos.size).times.map{ TransactionMemo.new  }
    vm.entries = vm.entries + missing_entries
    vm.memos   = vm.memos   + missing_memos
    render action: :edit
  end

  def destroy
    Transaction.filter(transaction_id: params[:id]).delete
    redirect_to transactions_url, notice: "Trasnaction deleted"
  end

  private

  attr_reader :bank_transaction, :candidates, :reconciliation
  helper_method :bank_transaction, :candidates, :reconciliation

  TRANSACTION_ENTRIES_AVAILBLE = 5
  TRANSACTION_MEMOS_AVAILABLE  = 30

  def candidates(account)
    @candidates ||= Hash.new
    @candidates[account] ||=
      begin
        ds = TransactionEntry.naked.
          join(Transaction, [:transaction_id]).
          left_join(ReconciledTransaction, [:transaction_id, :account]).
          filter(account: account, posted_on: bank_transaction.posted_on - 31 .. bank_transaction.posted_on + 31, reconciled_on: nil).
          select(:transaction_id, :posted_on, :description, :amount_dt, :amount_ct).
          order(:posted_on)

        entry = vm.entries.first
        ds = if entry.amount_dt.nonzero? then
               ds.filter(amount_dt: entry.amount_dt)
             else
               ds.filter(amount_ct: entry.amount_ct)
             end

        ds.to_a
      end
  end
end
