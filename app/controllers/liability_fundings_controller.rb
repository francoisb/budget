class LiabilityFundingsController < ApplicationController
  def index
    @vm = LiabilityFundingReport.new
  end

  def show
    @vm = LiabilityFundingTransactionEntriesReport.new(account_id: params[:id])
  end

  def new
    account = Account[account_id: params[:account_id]]
    raise "not found" unless account

    @liability_funding = LiabilityFundingForm.new(LiabilityFunding.new(account: account.account))
    if @liability_funding.validate(params[:liability_funding]) then
      amount = TransactionEntry.naked.
        filter(transaction_id: @liability_funding.entries.map(&:transaction_id)).
        select{ sum(amount_ct).as(:amount) }.
        first.fetch(:amount)

      @vm = TransactionForm.new(
        Transaction.new(
          transaction_id: SecureRandom.uuid,
          posted_on: Date.today,
          booked_on: Date.today,
          description: "Remboursement de dettes sur carte de crédit",
          entries: 4.times.map{ TransactionEntry.new }.unshift(TransactionEntry.new(account: account.account, amount_dt: amount, amount_ct: BigDecimal(0))),
          memos: 30.times.map{ TransactionMemo.new }))
      render "transactions/new"
    else
      raise "TODO on error"
    end
  end

  attr_reader :bank_transaction
  helper_method :bank_transaction
end
