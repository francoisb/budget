class PostingRulesController < ApplicationController
  def index
    @vm = PostingRulesReport.new(params[:posting_rules_report])
  end

  def new
    @vm = PostingRuleForm.new(PostingRule.new)
    vm.fixed_amount_rules = 30.times.map { FixedAmountPostingRuleEntry.new }
    vm.fixed_rate_rules   = 30.times.map { FixedRatePostingRuleEntry.new }
  end

  def create
    @vm = PostingRuleForm.new(PostingRule.new)
    vm.fixed_amount_rules = []
    vm.fixed_rate_rules = []
    write :new
  end

  def edit
    posting_rule = PostingRule[posting_rule_id: params[:id]]
    raise "not found" unless posting_rule

    @vm = PostingRuleForm.new(posting_rule)
    vm.fixed_amount_rules = posting_rule.fixed_amount_rules.concat(5.times.map { FixedAmountPostingRuleEntry.new })
    vm.fixed_rate_rules   = posting_rule.fixed_rate_rules.concat(  5.times.map { FixedRatePostingRuleEntry.new })
  end

  def update
    posting_rule = PostingRule[posting_rule_id: params[:id]]
    raise "not found" unless posting_rule

    @vm = PostingRuleForm.new(posting_rule)
    write :edit
  end

  def write(target)
    if vm.validate(params[:posting_rule]) then
      DB.transaction do
        vm.sync
        vm.model.save

        rules = vm.fixed_amount_rules.map{|rule| [vm.posting_rule, rule.memo_account, rule.amount]}
        FixedAmountPostingRuleEntry.filter(posting_rule: vm.posting_rule).delete
        FixedAmountPostingRuleEntry.import([:posting_rule, :memo_account, :amount], rules.reject{|row| row.second.blank? || row.last.blank? || BigDecimal(row.last.to_s).zero?})

        rules = vm.fixed_rate_rules.map{|rule| [vm.posting_rule, rule.memo_account, rule.rate]}
        FixedRatePostingRuleEntry.filter(posting_rule: vm.posting_rule).delete
        FixedRatePostingRuleEntry.import([:posting_rule, :memo_account, :rate], rules.reject{|row| row.second.blank?  || row.last.blank? || BigDecimal(row.last.to_s).zero?})
      end

      redirect_to posting_rules_url, notice: "Posting rule successfully updated"
    else
      render action: target
    end
  end
  private :write
end
