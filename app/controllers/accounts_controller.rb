class AccountsController < ApplicationController
  def index
    @vm = AccountsReport.new(params[:accounts_report])
    @account = AccountForm.new(Account.new)
  end

  def create
    @account = AccountForm.new(Account.new)
    if @account.validate(params[:account]) then
      @account.save
      redirect_to accounts_path
    else
      render action: :new
    end
  end

  def edit
    @account = AccountForm.new(Account[account_id: params[:id]])
    render
  end

  def update
    @account = AccountForm.new(Account[account_id: params[:id]])
    if @account.validate(params[:account]) then
      @account.save
      redirect_to accounts_path
    else
      render action: :edit
    end
  end

  def show
    @account = Account[account_id: params[:id]]
    raise "not found" unless @account

    @vm = TransactionEntriesReport.new(@account, params[:transaction_entries_report])
  end
end
