class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :require_auth unless Rails.env.development? || Rails.env.test?
  before_filter :set_locale

  helper_method :vm, :form

  private

  def require_auth
    authenticate_or_request_with_http_basic do |id, password|
      id == ENV["APP_USER"] && password == ENV["APP_PASSWORD"]
    end
  end

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  attr_reader :vm, :form

  def bank_accounts_for_select
    @bank_accounts_for_select ||= DB.from(BankAccount.naked.distinct.select(:account)).order{ lower(account) }.select_map(:account)
  end
  helper_method :bank_accounts_for_select

  def asset_accounts_for_select
    @asset_accounts_for_select ||= Account.naked.filter(account_kind: "asset").order{ lower(:account) }.select_map(:account)
  end
  helper_method :asset_accounts_for_select

  def accounts_for_select
    @accounts_for_select ||= Account.naked.order{ [:account_kind, lower(:account)] }.select_map(:account)
  end
  helper_method :accounts_for_select

  def memo_accounts_for_select
    @memo_accounts_for_select ||= DB.from(MemoAccount.naked.
      left_join(TransactionMemo, [:memo_account]).
      group_by(:memo_account).
      order{ lower(:memo_account) }.
      select(:memo_account, Sequel.lit("coalesce(sum(amount_dt) - sum(amount_ct), 0)").as(:balance))).select_hash(:memo_account, :balance)
  end
  helper_method :memo_accounts_for_select
end
