class RevenuesController < ApplicationController
  def new
    @form = RevenueForm.new(PostingRuleRun.new)
    form.posted_on = Date.today
    form.booked_on = Date.today

    if params.has_key?(:posting_rule_run) && form.validate(params[:posting_rule_run]) then
      posting_rule = PostingRule[posting_rule_id: form.posting_rule_id]
      raise "not found" unless posting_rule

      entries = 4.times.map{ TransactionEntry.new }.unshift(TransactionEntry.new(account: form.account, amount_dt: form.amount))
      memos   = posting_rule.memos_for_amount(form.account, form.amount) << TransactionMemo.new << TransactionMemo.new << TransactionMemo.new << TransactionMemo.new << TransactionMemo.new

      # 2nd step, we have an account now
      @vm = TransactionForm.new(Transaction.new(
        posted_on: form.posted_on,
        booked_on: form.booked_on,
        description: posting_rule.posting_rule,
        entries: entries,
        memos: memos))

      vm.posting_rule_id      = form.posting_rule_id
      vm.posting_rule_account = form.account
      vm.revenue_amount       = form.amount
    else
      logger.info form.errors.full_messages.inspect
    end
  end

  def posting_rules_for_select
    @posting_rules_for_select ||= PostingRule.naked.order{ lower(posting_rule) }.select_hash(:posting_rule, :posting_rule_id)
  end
  private :posting_rules_for_select
  helper_method :posting_rules_for_select
end
