class ReconciliationsController < ApplicationController
  def index
    @vm   = ReconciliationsReport.new(params[:reconciliations_report])
    @form = ReconciliationForm.new(Reconciliation.new(booked_on: Date.today))
  end

  def create
    @vm = ReconciliationForm.new(Reconciliation.new(booked_on: Date.today))
    if vm.validate(params[:reconciliation]) then
      vm.save
      redirect_to reconciliation_url(vm.model.reconciliation_id)
    else
      raise "TODO"
    end
  end

  def show
    reconciliation = Reconciliation[reconciliation_id: params[:id]]
    raise "not found" unless reconciliation

    account = Account[account: reconciliation.account]
    raise "not found" unless account

    entries = TransactionEntry.
      naked.
      join(Transaction, [:transaction_id]).
      left_join(ReconciliationEntry, [:transaction_id, :account, :amount_dt, :amount_ct]).
      filter(account: reconciliation.account).
      filter("statement_posted_on IS NULL OR statement_posted_on = ?", reconciliation.statement_posted_on).
      order(:posted_on, :booked_on, :transaction_id).
      select{ [reconciled_on, posted_on, description, amount_dt, amount_ct, transaction_id] }.
      map do |row|
        Entry.new(
          row.fetch(:reconciled_on),
          row.fetch(:posted_on),
          row.fetch(:description),
          row.fetch(:amount_dt),
          row.fetch(:amount_ct),
          row.fetch(:transaction_id))
      end

    @vm = ReconciliationForm.new(reconciliation)
    vm.entries = entries

    old_balance = TransactionEntry.naked.
      join(ReconciliationEntry, [:account, :transaction_id, :amount_dt, :amount_ct]).
      exclude("statement_posted_on >= ?", reconciliation.statement_posted_on).
      filter(account: reconciliation.account).
      select{ [sum(amount_dt).as(:amount_dt), sum(amount_ct).as(:amount_ct)] }.
      first

    new_balance = TransactionEntry.naked.
      join(ReconciliationEntry, [:account, :transaction_id, :amount_dt, :amount_ct]).
      filter("statement_posted_on <= ?", reconciliation.statement_posted_on).
      filter(account: reconciliation.account).
      select{ [sum(amount_dt).as(:amount_dt), sum(amount_ct).as(:amount_ct)] }.
      first

    zero = BigDecimal(0)
    case account.account_kind
    when "asset"
      vm.old_balance = (old_balance.fetch(:amount_dt) || zero) - (old_balance.fetch(:amount_ct) || zero)
      vm.new_balance = (new_balance.fetch(:amount_dt) || zero) - (new_balance.fetch(:amount_ct) || zero)
    when "liability"
      vm.old_balance = (old_balance.fetch(:amount_ct) || zero) - (old_balance.fetch(:amount_dt) || zero)
      vm.new_balance = (new_balance.fetch(:amount_ct) || zero) - (new_balance.fetch(:amount_dt) || zero)
    else
      raise "Cannot reconcile account #{account.name.inspect}: the account must be an asset or liability account, not a #{account.account_kind.inspect}"
    end
  end

  def update
    reconciliation = Reconciliation[reconciliation_id: params[:id]]
    raise "not found" unless reconciliation

    today = Date.today
    DB.transaction do
      # Remove everything...
      ReconciliationEntry.
        filter(statement_posted_on: reconciliation.statement_posted_on, account: reconciliation.account).
        delete

      # Then add everything that's supposed to be there
      entries = params[:reconciliation][:entries_attributes].
        select{|_, fragment| fragment[:transaction_id].present?}.
        values
      transaction_ids = entries.map{|fragment| fragment.fetch(:transaction_id)}
      txn_amounts = TransactionEntry.filter(transaction_id: transaction_ids, account: reconciliation.account).select_hash_groups(:transaction_id, [:amount_dt, :amount_ct])
      new_entries = transaction_ids.map do |transaction_id|
        amount_dt, amount_ct = *txn_amounts.fetch(transaction_id).flatten
        [reconciliation.account, reconciliation.statement_posted_on, transaction_id, today, amount_dt, amount_ct]
      end

      ReconciliationEntry.import([:account, :statement_posted_on, :transaction_id, :reconciled_on, :amount_dt, :amount_ct], new_entries)
    end

    redirect_to reconciliation_url(id: reconciliation.reconciliation_id)
  end

  Entry = Struct.new(:reconciled_on, :posted_on, :description, :amount_dt, :amount_ct, :transaction_id) do
    def persisted?
      false
    end
  end
end
