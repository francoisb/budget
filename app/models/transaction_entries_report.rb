class TransactionEntriesReport
  include Datagrid

  def initialize(*args)
    @account = args.shift
    params = args.first || {}
    super(params.merge(account_id: @account.account_id))
  end

  scope do
    TransactionEntry.naked.
      join(Transaction, [:transaction_id]).
      join(Account, [:account]).
      left_join(ReconciledTransaction, [:transaction_id, :account]).
      order{ [posted_on, transaction_entry_id] }
  end

  filter :account_id, :enum, select: Account.order{ [account_kind, lower(account)] }.select_hash(:account, :account_id) do |value|
    filter(account_id: value)
  end

  filter :posted_on, :date, range: true do |dates|
    if dates[0].present? && dates[1].present? then
      self.filter(posted_on: dates[0]..dates[1])
    elsif dates[0].blank? && dates[1].blank? then
      self
    elsif dates[0].blank? then
      DB.from(self.filter("posted_on <= ?", dates[1]).order{ [lower(account), posted_on.desc] }).
        order{ [lower(account), posted_on] }
    else
      self.filter("posted_on >= ?", dates[0])
    end
  end

  filter :description, :string do |value|
    self.grep([:description], "%#{value.to_s.strip.chomp}%", case_insensitive: true)
  end

  column :posted_on do |row|
    format(row.fetch(:posted_on)) do |value|
      l value, format: :iso8601
    end
  end

  column :description do |row|
    format(row.fetch(:description)) do |value|
      link_to value, edit_transaction_url(row.fetch(:transaction_id))
    end
  end

  column :amount_dt do |row|
    format(row.fetch(:amount_dt)) do |value|
      amount value
    end
  end

  column :amount_ct do |row|
    format(row.fetch(:amount_ct)) do |value|
      amount value
    end
  end

  column :reconciled do |row|
    !!row.fetch(:reconciled_on)
  end
end
