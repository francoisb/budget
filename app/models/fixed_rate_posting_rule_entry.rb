class FixedRatePostingRuleEntry < Sequel::Model
  def pct_rate
    rate ? rate * 100.0 : nil
  end

  def pct_rate=(value)
    self.rate = value ? BigDecimal(value) / 100.0 : nil
  end
end
