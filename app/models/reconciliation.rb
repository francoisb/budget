class Reconciliation < Sequel::Model(:reconciliations)
  one_to_many :entries, class: ReconciliationEntry, key: [:account, :statement_posted_on]
end
