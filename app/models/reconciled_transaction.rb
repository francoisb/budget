class ReconciledTransaction < Sequel::Model
  unrestrict_primary_key

  one_to_one :transaction, primary_key: :transaction_id
  one_to_one :bank_transaction, primary_key: :bank_transaction_id
end
