class Account < Sequel::Model
  unrestrict_primary_key

  alias_method :name, :account
  alias_method :name=, :account=
  alias_method :kind, :account_kind
  alias_method :kind=, :account_kind=

  def self.create_asset_account(name)
    create(account: name, account_kind: "asset")
  end

  def self.create_liability_account(name)
    create(account: name, account_kind: "liability")
  end

  def self.create_revenue_account(name)
    create(account: name, account_kind: "revenue")
  end

  def self.create_expense_account(name)
    create(account: name, account_kind: "expense")
  end

  def self.create_equity_account(name)
    create(account: name, account_kind: "equity")
  end

  def self.fuzzy_find_by_name(q)
    regexp = q.gsub(/[aâäàeêëéiîïoôöuûü]/) do |match|
      case match
      when /[aâäà]/ ; "[aâäà]"
      when /[eêëé]/ ; "[eêëé]"
      when /[iîï]/ ; "[iîï]"
      when /[oôö]/ ; "[oôö]"
      when /[uûü]/ ; "[uûü]"
      end
    end

    Account.filter("account ~* ?", regexp).order{ lower(account) }
  end
end
