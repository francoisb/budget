class AccountsReport
  include Datagrid

  scope do
    Account.naked.
      join(TransactionEntry, [:account]).
      select_all(:accounts).
      select_append{ (sum(amount_dt) - sum(amount_ct)).as(:current_balance) }.
      group_by(:account, :account_kind, :minimum_balance).
      order{ [account_kind, lower(account)] }
  end

  filter(:account)      {|value| self.grep([:account], "%#{value}%", case_insensitive: true) }
  filter(:account_kind, :enum, select: %w(asset liability equity revenue expense)) do |value|
    self.filter(account_kind: value)
  end

  column :account do |row|
    format(row.fetch(:account)) do |value|
      edit_link = link_to 'Edit', edit_account_url(row.fetch(:account_id)), class: "edit"
      report_link = link_to value, account_url(row.fetch(:account_id))
      edit_link << " " << report_link
    end
  end

  column :current_balance do |row|
    format(row.fetch(:current_balance)) do |value|
      amount value
    end
  end

  column :minimum_balance do |row|
    format(row.fetch(:minimum_balance)) do |value|
      amount value
    end
  end

  def column_class(row)
    "balance-error" if row.fetch(:minimum_balance) && row.fetch(:current_balance) < row.fetch(:minimum_balance)
  end

  column :account_kind do |row|
    row.fetch(:account_kind)
  end
end
