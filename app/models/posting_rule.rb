class PostingRule < Sequel::Model
  unrestrict_primary_key

  one_to_many :runs, class: PostingRuleRun, key: :posting_rule

  def fixed_amount_rules_dataset
    FixedAmountPostingRuleEntry.filter(posting_rule: posting_rule).order{ lower(memo_account) }
  end

  def fixed_amount_rules
    fixed_amount_rules_dataset.to_a
  end

  def fixed_rate_rules_dataset
    FixedRatePostingRuleEntry.filter(posting_rule: posting_rule).order{ lower(memo_account) }
  end

  def fixed_rate_rules
    fixed_rate_rules_dataset.to_a
  end

  attr_writer :fixed_amount_rules, :fixed_rate_rules

  def memos_for_amount(account, amount)
    base_amount = fixed_amount_rules.map(&:amount).reduce(BigDecimal(0), &:+)
    raise "Revenue of #{amount.to_s("F")} is too low for the base amount (#{base_amount.to_s("F")}) required for this posting rule" if base_amount > amount

    fixed_amount_memos = fixed_amount_rules.map do |rule|
      TransactionMemo.new(account: account, memo_account: rule.memo_account, amount_dt: rule.amount, amount_ct: BigDecimal(0))
    end

    distributable_amount = amount - base_amount
    fixed_rate_memos = fixed_rate_rules_dataset.naked.select{ [memo_account, trunc(rate * distributable_amount, 2).as(:amount_dt)] }.map do |row|
      TransactionMemo.new(
        account: account,
        memo_account: row.fetch(:memo_account),
        amount_dt: row.fetch(:amount_dt),
        amount_ct: BigDecimal(0))
    end

    pennies = distributable_amount - fixed_rate_memos.map(&:amount_dt).reduce(BigDecimal(0), &:+)
    fixed_rate_memos = fixed_rate_memos.map do |memo|
      if pennies.zero? then
        memo
      else
        pennies = pennies - BigDecimal("0.01")
        TransactionMemo.new(
          account: memo.account,
          memo_account: memo.memo_account,
          amount_dt: memo.amount_dt + 0.01,
          amount_ct: memo.amount_ct)
      end
    end

    fixed_amount_memos + fixed_rate_memos
  end
end
