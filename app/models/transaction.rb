class Transaction < Sequel::Model
  unrestrict_primary_key

  plugin :delay_add_association

  one_to_many :entries, class: TransactionEntry, key: :transaction_id
  one_to_many :memos,   class: TransactionMemo,  key: :transaction_id

  def entries=(es)
    TransactionEntry.filter(transaction_id: transaction_id).delete if persisted?
    es.each(&method(:add_entry))
  end

  def memos=(ms)
    TransactionMemo.filter(transaction_id: transaction_id).delete if persisted?
    ms.each(&method(:add_memo))
  end
end
