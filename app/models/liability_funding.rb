class LiabilityFunding < Sequel::Model
  one_to_many :entries, class: LiabilityFundingEntry, key: [:account, :funding_transaction_id]

  def entries=(rows)
    rows.each{|row| add_entry row}
  end
end
