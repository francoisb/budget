require "csv"
require "digest/sha2"

class BankTransactionParser
  def self.parser_class_from_format(format)
    case format
    when "desjardins" ; DesjardinsParser
    when "rbc"        ; RbcParser
    else
      raise ArgumentError, "Unknown bank transaction parser format: #{format.inspect}"
    end
  end

  def initialize(io)
    @io = io
    @line = 1
  end

  attr_reader :io, :line

  def parse
    transactions = []
    DB.transaction do
      transactions = build_csv(io).reject(&:empty?).reject{|row| row[0] == "\u0000"}.map do |row|
        @line += 1
        map_to_bank_transaction_row(row).unshift SecureRandom.uuid
      end

      today = Date.today
      desired_account_number_hashes = transactions.map{|row| row[1]}.uniq
      existing_account_number_hashes = BankAccount.filter(account_number_hash: desired_account_number_hashes).select_map(:account_number_hash)
      if desired_account_number_hashes.sort == existing_account_number_hashes.sort then
        DB.run "CREATE TEMPORARY TABLE bank_transactions_import(LIKE bank_transactions EXCLUDING CONSTRAINTS) ON COMMIT DROP"
        DB[:bank_transactions_import].import(
          [:bank_transaction_id, :account_number_hash, :posted_on, :description, :amount, :booked_on],
          transactions.map{|row| row << today})
        DB[:bank_transactions].insert(
          DB[:bank_transactions_import].
          distinct(:account_number_hash, :posted_on, :description, :amount).
          left_join(:bank_transactions, [:account_number_hash, :posted_on, :description, :amount]).
          select_all(:bank_transactions_import).
          filter(bank_transactions__account_number_hash: nil))
      else
        missing_account_number_hashes = desired_account_number_hashes - existing_account_number_hashes
        STDERR.puts "Missing account number hashes!"
        missing_account_number_hashes.each do |missing_account_number_hash|
          transaction = transactions.detect{|row| row[1] == missing_account_number_hash}
          STDERR.puts "\t#{transaction.inspect}"
        end
      end
    end

    transactions.size
  rescue Sequel::UniqueConstraintViolation => e
    raise TransactionAlreadyExists, e.message
  end

  class BankAccountNotFound < RuntimeError ; end
  class UnrecognizedBankTransactionFormatError < ArgumentError ; end
  class TransactionAlreadyExists < RuntimeError ; end

  class DesjardinsParser < BankTransactionParser
    def build_csv(io)
      CSV(io, col_sep: ",", headers: false)
    end

    def map_to_bank_transaction_row(row)
      account_number_hash = Digest::SHA256.hexdigest([row[0], row[1], row[2]].join(":"))
      amount = case
               when !row[7].blank?
                 BigDecimal(row[7]) * -1
               when !row[8].blank?
                 BigDecimal(row[8])

               when !row[11].blank?
                 BigDecimal(row[11]) # add to credit, hence this is money we owe to the bank (credit the credit card account)
               when !row[12].blank?
                 BigDecimal(row[12]) * -1 # paying down a credit card, hence we owe less money to the bank (debit the credit card account)
               else
                 raise UnrecognizedBankTransactionFormatError, "Unable to find amount from row: #{(0 ... row.length).map{|n| row[n]}.inspect}"
               end
      [account_number_hash,
       row[3].to_date,
       row[5].to_s.gsub(/  */, " "),
       BigDecimal(amount)]
    end
  end

  class RbcParser < BankTransactionParser
    def build_csv(io)
      CSV(io, col_sep: ",", headers: true)
    end

    def accounts
      @accounts ||= BankAccount.join(Account, [:account]).naked.select_hash(:account_number_hash, :account_kind)
    end

    def map_to_bank_transaction_row(row)
      account_number_hash = Digest::SHA256.hexdigest([row[0], row[1]].join(":"))

      month, day, year = row[2].split("/").map(&:to_i)
      raise UnrecognizedBankTransactionFormatError, "Expected MM/DD/YYYY date format but found #{row[2].inspect}" unless year >= 2010

      amount = case accounts.fetch(account_number_hash)
               when "asset"
                 BigDecimal(row[6])
               when "liability"
                 # RBC exports credit advances as negative amounts: we want positive ones, in order to express the correct relationship:
                 # making a payment means reducing our liability, while using the credit card means increasing our liability
                 -1 * BigDecimal(row[6])
               else
                 raise "ASSERTION ERROR: Unrecognized account kind for account #{accounts.fetch(account_number_hash).inspect}"
               end

      [account_number_hash,
       Date.new(year, month, day),
       [row[4], row[5]].join(" ").strip.gsub(/  */, " "),
       amount]
    rescue KeyError => e
      raise BankAccountNotFound, e.message
    rescue NoMethodError, ArgumentError => e
      raise UnrecognizedBankTransactionFormatError, "#{e.class} - #{e.message}: failed to parse line #{line}: #{row.inspect}"
    end
  end
end
