class PostingRuleRun < Sequel::Model
  set_primary_key [:posting_rule, :transaction_id]
  unrestrict_primary_key

  many_to_one :posting_rule, key: :posting_rule
  many_to_one :transaction, key: :transaction_id
end
