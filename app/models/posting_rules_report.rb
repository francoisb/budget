class PostingRulesReport
  include Datagrid

  scope do
    PostingRule.naked.
      select{ [posting_rule_id, posting_rule, coalesce(fixed_amount, 0).as(:fixed_amount), coalesce(fixed_rate * 100, 0).as(:fixed_rate)] }.
      left_join(FixedAmountPostingRuleEntry.group(:posting_rule).select{ [posting_rule, sum(amount).as(:fixed_amount)] }, [:posting_rule]).
      left_join(FixedRatePostingRuleEntry.group(:posting_rule).select{ [posting_rule, sum(rate).as(:fixed_rate)] }, [:posting_rule]).
      order{ lower(posting_rule) }
  end

  column :posting_rule do |row|
    format(row.fetch(:posting_rule)) do |value|
      link_to value, edit_posting_rule_url(row.fetch(:posting_rule_id))
    end
  end

  column :fixed_amount do |row|
    format(row.fetch(:fixed_amount)) do |value|
      amount value
    end
  end

  column :fixed_rate do |row|
    format(row.fetch(:fixed_rate)) do |value|
      number_to_percentage value, precision: 2
    end
  end
end
