class MemoAccount < Sequel::Model
  unrestrict_primary_key

  def self.by_memo_account
    ds = MemoAccount.naked.
      join(TransactionMemo, [:memo_account]).
      group(:memo_account).
      select(:memo_account, Sequel.lit("count(distinct account)").as(:rowspan))

    MemoAccount.naked.
      left_join(TransactionMemo, [:memo_account]).
      left_join(Transaction, [:transaction_id]).
      left_join(ds, [:memo_account]).
      left_join(Account, [:account]).
      group(:memo_account_id, :account_id, :memo_account, :account, :rowspan).
      order{ [lower(memo_account), lower(account)] }.
      select{ [memo_account_id, account_id, memo_account, account, max(posted_on).as(:posted_on), (sum(amount_dt) - sum(amount_ct)).as(:balance), :rowspan] }.
      having(Sequel.lit("(sum(amount_dt) - sum(amount_ct)) <> 0")).
      select_append(Sequel.lit("row_number() over (partition by memo_account order by memo_account, account)").as(:row_number))
  end

  def self.by_account
    ds = MemoAccount.naked.
      join(TransactionMemo, [:memo_account]).
      group(:account).
      select(:account, Sequel.lit("count(distinct memo_account)").as(:rowspan))

    MemoAccount.naked.
      left_join(TransactionMemo, [:memo_account]).
      left_join(Account, [:account]).
      left_join(Transaction, [:transaction_id]).
      left_join(ds, [:account]).
      group(:memo_account_id, :account_id, :account, :memo_account, :rowspan).
      order{ [lower(account), lower(memo_account)] }.
      exclude(account: nil).
      having(Sequel.lit("(sum(amount_dt) - sum(amount_ct)) <> 0")).
      select{ [memo_account_id, account_id, account, memo_account, max(posted_on).as(:posted_on), (sum(amount_dt) - sum(amount_ct)).as(:balance), :rowspan] }.
      select_append(Sequel.lit("row_number() over (partition by account order by account, memo_account)").as(:row_number))
  end
end
