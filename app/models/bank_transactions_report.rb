class BankTransactionsReport
  include Datagrid

  scope do
    BankTransaction.
      naked.
      join(BankAccount, [:account_number_hash]).
      left_join(ReconciledTransaction, [:bank_transaction_id, :account]).
      order{ [lower(account), posted_on.desc, bank_transaction_id] }
  end

  filter :posted_on, :date, range: true do |dates|
    if dates[0].present? && dates[1].present? then
      self.filter(posted_on: dates[0]..dates[1])
    elsif dates[0].blank? && dates[1].blank? then
      self
    elsif dates[0].blank? then
      DB.from(self.filter("posted_on <= ?", dates[1]).order{ [lower(account), posted_on.desc] }).
        order{ [lower(account), posted_on] }
    else
      self.filter("posted_on >= ?", dates[0])
    end
  end

  filter :description, :string do |value|
    self.grep([:description], "%#{value.to_s.strip.chomp}%", case_insensitive: true)
  end

  filter(:bank_account_id, :enum, select: BankAccount.naked.order{ lower(account) }.select_hash(:account, :bank_account_id)) do |value|
    bank_accounts_ds       = BankAccount.select(:account).filter(bank_account_id: value)
    account_number_hash_ds = BankAccount.select(:account_number_hash).filter(account: bank_accounts_ds)
    self.filter(account_number_hash: account_number_hash_ds)
  end

  filter(:reconciled, :xboolean) do |value|
    case value
    when false
      self.filter(reconciled_transactions__bank_transaction_id: nil)
    when true
      self.exclude(reconciled_transactions__bank_transaction_id: nil)
    else
      self
    end
  end

  column(:account) do |row|
    row.fetch(:account)
  end

  column(:posted_on) do |row|
    format(row.fetch(:posted_on).to_date) do |value|
      l value, format: :iso8601
    end
  end

  column(:description) do |row|
    format(row.fetch(:description)) do |value|
      target_url = if !!row.fetch(:reconciled_on) then
              edit_transaction_url(id: row.fetch(:transaction_id))
            else
              new_transaction_from_bank_transaction_url(id: row.fetch(:bank_transaction_id))
            end

      link_to value, target_url
    end
  end

  column(:amount) do |row|
    format(row.fetch(:amount)) do |value|
      amount value
    end
  end

  column(:reconciled) do |row|
    !!row.fetch(:reconciled_on)
  end
end
