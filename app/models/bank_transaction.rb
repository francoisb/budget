class BankTransaction < Sequel::Model
  unrestrict_primary_key
  many_to_one :reconciled_transaction, key: :bank_transaction_id
  many_to_one :bank_account, key: :account_number_hash

  delegate :account, to: :bank_account
end
