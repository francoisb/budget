class ReconciliationsReport
  include Datagrid

  scope do
    old_balance_ds = TransactionEntry.naked.
      join(ReconciliationEntry, [:account, :transaction_id, :amount_dt, :amount_ct]).
      group(:account, :statement_posted_on).
      select{ [account, statement_posted_on, sum(amount_dt).as(:old_amount_dt), sum(amount_ct).as(:old_amount_ct)] }

    new_balance_ds = TransactionEntry.naked.
      join(ReconciliationEntry, [:account, :transaction_id, :amount_dt, :amount_ct]).
      group(:account, :statement_posted_on).
      select{ [account, statement_posted_on, sum(amount_dt).as(:new_amount_dt), sum(amount_ct).as(:new_amount_ct)] }

    Reconciliation.naked.
      join(old_balance_ds, [:account, :statement_posted_on]).
      join(new_balance_ds, [:account, :statement_posted_on]).
      join(Account, [:account]).
      order{ [statement_posted_on.desc, lower(account)] }
  end

  filter(:account, :enum, select: BankAccount.naked.order{ lower(account) }.select_map(:account)) do |value|
    self.filter(account: value)
  end

  column :statement_posted_on do |row|
    format(row.fetch(:statement_posted_on)) do |value|
      link_to l(value.to_date, format: :iso8601), reconciliation_url(row.fetch(:reconciliation_id))
    end
  end

  column :account do |row|
    row.fetch(:account)
  end

  # column :old_balance do |row|
  #   balance =
  #     case row.fetch(:account_kind)
  #     when "asset"
  #       row.fetch(:old_amount_dt) - row.fetch(:old_amount_ct)
  #     when "liability"
  #       row.fetch(:old_amount_ct) - row.fetch(:old_amount_dt)
  #     else
  #       raise "ASSERTION ERROR: Only asset and liability accounts can be reconciled, not #{row.inspect}"
  #     end

  #   format(balance) do |value|
  #     amount value
  #   end
  # end

  column :calculated_balance do |row|
    balance =
      case row.fetch(:account_kind)
      when "asset"
        row.fetch(:new_amount_dt) - row.fetch(:new_amount_ct)
      when "liability"
        row.fetch(:new_amount_ct) - row.fetch(:new_amount_dt)
      else
        raise "ASSERTION ERROR: Only asset and liability accounts can be reconciled, not #{row.inspect}"
      end

    format(balance) do |value|
      amount value
    end
  end

  column :expected_balance do |row|
    format(row.fetch(:balance)) do |value|
      amount value
    end
  end

  column :balance_difference do |row|
    expected_balance = row.fetch(:balance)
    actual_balance =
      case row.fetch(:account_kind)
      when "asset"
        row.fetch(:new_amount_dt) - row.fetch(:new_amount_ct)
      when "liability"
        row.fetch(:new_amount_ct) - row.fetch(:new_amount_dt)
      else
        raise "ASSERTION ERROR: Only asset and liability accounts can be reconciled, not #{row.inspect}"
      end

    format(expected_balance - actual_balance) do |value|
      amount value.abs
    end
  end

  def column_class(row)
    expected_balance = row.fetch(:balance)
    actual_balance =
      case row.fetch(:account_kind)
      when "asset"
        row.fetch(:new_amount_dt) - row.fetch(:new_amount_ct)
      when "liability"
        row.fetch(:new_amount_ct) - row.fetch(:new_amount_dt)
      else
        raise "ASSERTION ERROR: Only asset and liability accounts can be reconciled, not #{row.inspect}"
      end

    expected_balance == actual_balance ? nil : "balance-error"
  end
end
