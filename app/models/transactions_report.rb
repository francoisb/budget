class TransactionsReport
  include Datagrid

  scope do
    Transaction.
      join(TransactionEntry, [:transaction_id]).
      join(Account, [:account]).
      group_by(:posted_on, :booked_on, :description, :transaction_id).
      select(Sequel.lit("transactions.*")).
      select_append(Sequel.lit("sum(amount_dt)").as(:amount)).
      select_append(Sequel.lit("array_agg(account order by account_kind, lower(account))").as(:accounts)).
      order{ [posted_on.desc, booked_on.desc] }.
      naked
  end

  filter :posted_on, :date, range: true do |dates|
    dates[0] = Date.new(2000, 1, 1) if dates[0].blank?
    dates[1] = Date.today >> 6      if dates[1].blank?
    self.filter(posted_on: dates[0]..dates[1])
  end

  filter :description, :string do |description|
    self.grep([:description], "%#{description.to_s.strip.chomp}%", case_insensitive: true)
  end

  filter :amount, :float do |amount|
    self.filter(transaction_id: TransactionEntry.filter(amount_dt: amount).or(amount_ct: amount).naked.select(:transaction_id))
  end

  filter :account_id, :enum, select: Account.order{ [:account_kind, lower(:account)] }.select_hash(:account, :account_id) do |account_id|
    self.filter(transaction_id: TransactionEntry.filter(account: Account[account_id: account_id].account).naked.select(:transaction_id))
  end

  column(:posted_on, order: :transactions__posted_on) do |row|
    format(row.fetch(:posted_on).to_date) do |value|
      l value, format: :iso8601
    end
  end

  column(:description) do |row|
    format(row.fetch(:description)) do |value|
      link_to value, edit_transaction_url(id: row.fetch(:transaction_id))
    end
  end

  column(:amount) do |row|
    format(row.fetch(:amount)) do |value|
      amount value
    end
  end

  column(:accounts) do |row|
    format(row.fetch(:accounts)) do |value|
      value.to_sentence
    end
  end
end
