class LiabilityFundingReport
  include Datagrid

  scope do
    opening_balances_ds = DB.from(
      Transaction.naked.
      join(TransactionEntry, [:transaction_id]).
      join(Account, [:account]).
      filter(account_kind: "liability").
      select(:transaction_id, Sequel.lit("row_number() over (partition by account order by posted_on, booked_on, transaction_entry_id)").as(:row_number))).
    filter(row_number: 1).
    select(:transaction_id)

    liability_funded_ds = LiabilityFundingEntry.naked.select(:transaction_id)

    TransactionEntry.naked.
      join(Transaction, [:transaction_id]).
      join(Account, [:account]).
      filter(account_kind: 'liability', amount_dt: 0).
      exclude(transaction_id: opening_balances_ds).
      exclude(transaction_id: liability_funded_ds ).
      group(:account, :account_id).
      select{ [account, account_id, sum(amount_ct).as(:amount)] }.
      order{ lower(account) }
  end

  column :account do |row|
    format(row.fetch(:account)) do |value|
      link_to value, liability_funding_url(row.fetch(:account_id))
    end
  end

  column :amount do |row|
    format(row.fetch(:amount)) do |value|
      amount value
    end
  end
end
