class TransactionEntry < Sequel::Model
  set_primary_key [:transaction_id, :account]
  unrestrict_primary_key

  many_to_one :transaction, key: :transaction_id
end
