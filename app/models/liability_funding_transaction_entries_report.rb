class LiabilityFundingTransactionEntriesReport
  include Datagrid

  scope do
    opening_balances_ds = DB.from(
      Transaction.naked.
      join(TransactionEntry, [:transaction_id]).
      join(Account, [:account]).
      filter(account_kind: "liability").
      select(:transaction_id, Sequel.lit("row_number() over (partition by account order by posted_on, booked_on, transaction_entry_id)").as(:row_number))).
    filter(row_number: 1).
    select(:transaction_id)

    liability_funded_ds = LiabilityFundingEntry.naked.select(:transaction_id)

    TransactionEntry.naked.
      join(Transaction, [:transaction_id]).
      join(Account, [:account]).
      filter(account_kind: 'liability', amount_dt: 0).
      exclude(transaction_id: opening_balances_ds).
      exclude(transaction_id: liability_funded_ds).
      select(:account, :transaction_id, :posted_on, :description, :amount_dt, :amount_ct).
      order{ [lower(account), posted_on.desc, booked_on.desc, transaction_entry_id.desc] }
  end

  filter :account_id, :enum, select: BankAccount.naked.join(Account, [:account]).filter(account_kind: "liability").order{ lower(account) }.select_hash(:account, :account_id).to_a do |value|
    filter(account_id: value)
  end

  column :transaction_id, header: "" do |row|
    format(row.fetch(:transaction_id)) do |value|
      check_box_tag "liability_funding[entries][][transaction_id]", value, false
    end
  end

  column :account do |row|
    row.fetch(:account)
  end

  column :posted_on do |row|
    format(row.fetch(:posted_on)) do |value|
      l value, format: :iso8601
    end
  end

  column :description do |row|
    format(row.fetch(:description)) do |value|
      link_to value, edit_transaction_url(row.fetch(:transaction_id))
    end
  end

  column :amount_dt do |row|
    format(row.fetch(:amount_dt)) do |value|
      amount value
    end
  end

  column :amount_ct do |row|
    format(row.fetch(:amount_ct)) do |value|
      amount value
    end
  end
end
