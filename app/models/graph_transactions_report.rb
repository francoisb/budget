class GraphTransactionsReport
  include Datagrid

  scope do
    TransactionEntry.naked.
      join(Transaction, [:transaction_id]).
      join(Account, [:account]).
      group_by(:account_kind, :account).
      filter(account_kind: %w(expense revenue)).
      order{ [account_kind, (sum(amount_dt) + sum(amount_ct)).desc] }.
      select{ [account_kind, account, sum(amount_dt).as(:amount_dt), sum(amount_ct).as(:amount_ct)] }
  end

  filter :posted_on, :date, range: true do |dates|
    dates[0] = Date.new(2000, 1, 1) if dates[0].blank?
    dates[1] = Date.today >> 6      if dates[1].blank?
    self.filter(posted_on: dates[0]..dates[1])
  end

  column(:account_kind) {|row| row.fetch(:account_kind)}
  column(:account)      {|row| row.fetch(:account)}
  column(:amount_dt)    {|row| row.fetch(:amount_dt)}
  column(:amount_ct)    {|row| row.fetch(:amount_ct)}
end
