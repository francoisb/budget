class MemoAccountTransactionsReport
  include Datagrid

  scope do
    TransactionMemo.naked.
      join(Transaction, [:transaction_id]).
      select_all(:transaction_memos, :transactions).
      select_append(Sequel.lit("sum(amount_dt) over (partition by memo_account order by posted_on, booked_on, transaction_memo_id) - sum(amount_ct) over (partition by memo_account order by posted_on, booked_on, transaction_memo_id)").as(:balance)).
      order{ [posted_on.desc, booked_on.desc, transaction_memo_id.desc] }
  end

  filter(:account, :enum, select: BankAccount.naked.join(Account, [:account]).order{ lower(account) }.select_map(:account)) do |value|
    filter(account: value)
  end

  filter(:memo_account, :enum, select: MemoAccount.naked.order{ lower(memo_account) }.select_map(:memo_account)) do |value|
    filter(memo_account: value)
  end

  filter :posted_on, :date, range: true do |dates|
    dates[0] = Date.new(2000, 1, 1) if dates[0].blank?
    dates[1] = Date.today >> 6      if dates[1].blank?
    self.filter(posted_on: dates[0]..dates[1])
  end

  filter(:description) do |value|
    grep([:description], "%#{value}%", case_insensitive: true)
  end

  column :account do |row|
    row.fetch(:account)
  end

  column :posted_on do |row|
    format(row.fetch(:posted_on)) do |value|
      l value, format: :iso8601
    end
  end

  column :description do |row|
    format(row.fetch(:description)) do |value|
      link_to value, edit_transaction_url(row.fetch(:transaction_id))
    end
  end

  column :amount_dt do |row|
    format(row.fetch(:amount_dt)) do |value|
      amount value
    end
  end

  column :amount_ct do |row|
    format(row.fetch(:amount_ct)) do |value|
      amount value
    end
  end

  column :balance do |row|
    format(row.fetch(:balance)) do |value|
      amount value
    end
  end
end
