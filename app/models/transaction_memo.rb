class TransactionMemo < Sequel::Model
  set_primary_key [:transaction_id, :memo_account]
  unrestrict_primary_key

  many_to_one :transaction, key: :transaction_id
  many_to_one :memo, class: MemoAccount, key: :memo_account
end
