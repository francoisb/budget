class MemoAccountsReport
  include Datagrid

  scope do
    ds = MemoAccount.naked.
      join(TransactionMemo, [:memo_account]).
      group(:memo_account).
      select(:memo_account, Sequel.lit("count(distinct account)").as(:rowspan))

    MemoAccount.naked.
      left_join(TransactionMemo, [:memo_account]).
      left_join(Transaction, [:transaction_id]).
      left_join(ds, [:memo_account]).
      group(:memo_account, :account, :rowspan).
      order{ [lower(memo_account), lower(account)] }.
      select{ [memo_account, account, max(posted_on).as(:posted_on), (sum(amount_dt) - sum(amount_ct)).as(:balance), :rowspan] }.
      select_append(Sequel.lit("row_number() over (partition by memo_account order by memo_account, account)").as(:row_number))
  end

  filter(:name, type: :string) do |value|
    self.grep([:memo_account], "%#{value}%", case_insensitive: true)
  end

  column(:name) do |row|
    format(row.fetch(:memo_account)) do |value|
      link_to value, "#"
    end
  end

  column(:posted_on) do |row|
    format(row.fetch(:posted_on)) do |value|
      if value then
        l value.to_date, format: :iso8601
      else
        nil
      end
    end
  end

  column(:balance) do |row|
    format(row.fetch(:balance)) do |value|
      amount value
    end
  end
end
