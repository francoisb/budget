require "reform/form"
require "reform/form/coercion"

class PostingRuleForm < Reform::Form
  include Coercion

  model :posting_rule

  property :posting_rule_id, type: Integer, writeable: false
  property :posting_rule, type: String

  collection :fixed_amount_rules, skip_if: :all_blank, populate_if_empty: ->(fragment, _) { FixedAmountPostingRuleEntry.new(posting_rule: posting_rule) } do
    property :id, virtual: true
    property :memo_account, type: String
    property :amount, type: BigDecimal
  end

  collection :fixed_rate_rules, skip_if: :all_blank, populate_if_empty: ->(fragment, _) { FixedRatePostingRuleEntry.new(posting_rule: posting_rule) } do
    property :id, virtual: true
    property :memo_account, type: String
    property :pct_rate, type: BigDecimal

    def rate
      pct_rate ? BigDecimal(pct_rate) / 100 : nil
    end

    def rate=(value)
      self.pct_rate = value ? BigDecimal(value) / 100 : nil
    end
  end
end
