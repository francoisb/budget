class AccountForm < Reform::Form
  model :account

  property :account_id, readonly: true
  property :name, validates: {presence: true}
  property :minimum_balance, validates: {numericality: {greater_than_or_equal: 0, allow_blank: true, allow_nil: true}}
  property :kind, validates: {presence: true, inclusion: %w(asset liability revenue expense equity)}
end
