require "reform"
require "reform/form/coercion"

class TransactionForm < Reform::Form
  include Coercion

  model :transaction

  property :transaction_id, type: String, validates: {presence: true}, readonly: true
  property :bank_transaction_id, type: String, virtual: true
  property :posted_on, type: Date, validates: {presence: true}, readonly: :posted_on_readonly?
  property :booked_on, type: Date, validates: {presence: true}, readonly: true
  property :description, type: String, validates: {presence: true}
  property :account, type: String, virtual: true

  # When creating a posting rule run
  property :posting_rule_id, type: Integer, virtual: true
  property :posting_rule_account, type: String, virtual: true
  property :revenue_amount, type: BigDecimal, virtual: true

  collection :entries, populate_if_empty: TransactionEntry, skip_if: ->(fragment, _){ fragment["account"].blank? } do
    property :account, type: String
    property :amount_dt, type: BigDecimal
    property :amount_ct, type: BigDecimal
  end

  collection :memos, populate_if_empty: TransactionMemo, skip_if: ->(fragment, _){ fragment["memo_account"].blank? } do
    property :account, type: String
    property :memo_account, type: String
    property :amount_dt, type: BigDecimal
    property :amount_ct, type: BigDecimal
  end

  validate :sum_of_entries_balances
  validate :sum_of_memos_balances_transaction

  def posted_on_readonly?
    bank_transaction_id.present?
  end

  def sum_of_entries_balances
    amount_dt = entries.map(&:amount_dt).reject(&:blank?).map(&method(:BigDecimal)).reduce(BigDecimal(0), &:+)
    amount_ct = entries.map(&:amount_ct).reject(&:blank?).map(&method(:BigDecimal)).reduce(BigDecimal(0), &:+)
    return if amount_dt == amount_ct

    errors.add(:entries, "are unbalanced: sum of debits (#{amount_dt.to_s("F")}) does not equal sum of credits (#{amount_ct.to_s("F")})")
  end

  def sum_of_memos_balances_transaction
    # amount = memos.map(&:amount).reject(&:blank?).map(&method(:BigDecimal)).reduce(BigDecimal(0), &:+)
    # amount_dt = entries.map(&:amount_dt).reject(&:blank?).map(&method(:BigDecimal)).reduce(BigDecimal(0), &:+)
    # return if amount_dt == amount.abs || amount.zero?

    # errors.add(:memos, "are unbalanced: transaction has sum of #{amount_dt.to_s("F")} while sum of memos is #{amount.to_s("F")}")
  end
end
