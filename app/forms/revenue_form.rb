require "reform/form"
require "reform/form/coercion"

class RevenueForm < Reform::Form
  include Coercion

  model :posting_rule_run

  property :account, validates: {presence: true, inclusion: {in: ->(*){ Account.naked.filter(account_kind: "asset").select_map(:account) }}}
  property :amount, type: BigDecimal, validates: {presence: true, numericality: {greater_than: BigDecimal(0)}}
  property :posted_on, type: Date, virtual: true, validates: {presence: true}
  property :booked_on, type: Date, readonly: true, virtual: true, validates: {presence: true}
  property :posting_rule_id, type: Integer, virtual: true, validates: {presence: true, inclusion: {in: ->(*){ PostingRule.naked.select_map(:posting_rule_id) }}}
end
