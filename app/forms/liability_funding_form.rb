class LiabilityFundingForm < Reform::Form
  model :liability_funding

  property :account, validates: {presence: true}

  collection :entries, populate_if_empty: ->(fragment, _){ LiabilityFundingEntry.new(fragment) } do
    property :transaction_id, validates: {presence: true}
  end
end
