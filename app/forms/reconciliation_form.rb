require "reform"
require "reform/form/coercion"

class ReconciliationForm < Reform::Form
  include Coercion

  model :reconciliation

  property :account, type: String, validates: {presence: true}
  property :statement_posted_on, type: Date, validates: {presence: true}
  property :booked_on, type: Date, validates: {presence: true}, readonly: true
  property :balance, type: BigDecimal, validates: {presence: true, numericality: true}
  property :old_balance, type: BigDecimal, virtual: true
  property :new_balance, type: BigDecimal, virtual: true

  collection :entries, virtual: true do
    property :reconciled_on, type: Date, readonly: true
    property :posted_on, type: Date, readonly: true
    property :description, type: String, readonly: true
    property :amount_dt, type: BigDecimal, readonly: true
    property :amount_ct, type: BigDecimal, readonly: true
    property :transaction_id, type: String, validates: {presence: true}
  end
end
