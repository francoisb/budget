require "reform"
require "reform/form/coercion"

class ProcessingForm < Reform::Form
  model :transaction

  property :transaction_id, type: String, validates: {presence: true}, virtual: true
end
