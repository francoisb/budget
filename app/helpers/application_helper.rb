module ApplicationHelper
  def amount(number, options={})
    number_to_currency(number, {precision: 2, unit: ""}.merge(options)).try(:strip)
  end

  def active_if(controller, action=nil)
    actual = [params[:controller], action ? params[:action] : nil].map(&:to_s)
    desired = [controller, action].map(&:to_s)

    desired == actual ? :active : nil
  end

  def paginate(*args)
    options = args.extract_options!
    vm = args.first
    options = {key: :posted_on}.merge(options)
    report = vm.class.name.demodulize.underscore

    min_posted_on = nil
    max_posted_on = nil

    if vm.assets.empty? then
      # No assets / rows: use the dates in the URL
      if params.include?(report) then
        # We have one or more dates in the URL: use those
        posted_ons = params[report][options.fetch(:key)]
        if posted_ons && posted_ons.any?(&:present?) then
          min_posted_on = posted_ons.last
          max_posted_on = posted_ons.first
        else
          # We have data for the report, but not the dates. Do not render pagination
          return nil
        end
      else
        # No assets and no data in the URL? Don't render any pagination
        return nil
      end
    else
      min_posted_on = vm.assets.map{|row| row.fetch(options.fetch(:key))}.min
      max_posted_on = vm.assets.map{|row| row.fetch(options.fetch(:key))}.max
    end

    previous_page_url = url_for(params.merge(only_path: false, report => params.fetch(report, {}).merge(options.fetch(:key) => ["", min_posted_on])))
    current_page_url  = url_for(params.merge(only_path: false))
    next_page_url     = url_for(params.merge(only_path: false, report => params.fetch(report, {}).merge(options.fetch(:key) => [max_posted_on, ""])))

    locals = {
      min_posted_on: min_posted_on.blank? ? nil : min_posted_on.to_date,
      max_posted_on: max_posted_on.blank? ? nil : max_posted_on.to_date,
      previous_page_url: previous_page_url,
      current_page_url: current_page_url,
      next_page_url: next_page_url}

    render partial: "shared/pagination", locals: locals
  end

  def date_range_to_text(*args)
    options = args.extract_options!
    options = {format: :iso8601}.merge(options)

    min_date, max_date = *args[0, 2]

    case min_date
    when Date, Time, DateTime
      if min_date == max_date then
        "#{l(min_date, options)}".html_safe
      else
        case max_date
        when Date, Time, DateTime
          # We have both!
          "#{l(min_date, options)}&nbsp;-&nbsp;#{l(max_date, options)}".html_safe
        else
          # We only have min_date
          "&hellip;&nbsp;-&nbsp;#{l(min_date, options)}".html_safe
        end
      end
    else
      case max_date
      when Date, Time, DateTime
        # We only have max_date
        "#{l(max_date, options)}&nbsp;-&nbsp;&hellip;".html_safe
      else
        # We have no dates
        "&hellip;&nbsp;-&nbsp;&hellip;".html_safe
      end
    end
  end

  def size_per_col(filters)
    return [0] if filters.empty?

    sizes = filters.map do |filter|
      case filter.name.to_s
      when /_on$/        ; 3
      when /_id$/        ; 3
      when "description", "name" ; 4
      else               ; 2
      end
    end

    total_size = sizes.reduce(&:+)
    if total_size == 12 then
      sizes
    elsif total_size < 12 then
      # We can increase the size of certain fields without causing issues
      sizes = filters.zip(sizes).map do |filter, size|
        case filter.name.to_s
        when "description", "name" ; size + 1
        else                       ; size
        end
      end

      total_size = sizes.reduce(&:+)
      leftover = 12 - total_size # I would need an empty column of this size...
      sizes
    else
      # PANIC! We're building a UI with more than 12 columns
      # in width... whatever are we gonna do!
      sizes.map(&:pred)
    end
  end

  def flash_message_with_link(key)
    value = flash[key]
    link = flash["#{key}_link".to_sym]
    link.nil? ? value : string_with_link(value, link).html_safe
  end

  # Converts
  # "string with __link__ in the middle." to
  # "string with #{link_to('link', link_url, link_options)} in the middle."
  # --> see http://stackoverflow.com/questions/2543936/rails-i18n-translating-text-with-links-inside (holli)
  def string_with_link(str, link_url, link_options = {})
    match = str.match(/__([^_]{2,30})__/)
    raise "string_with_link: No place for __link__ given in #{str}" if match.blank?
    $` + link_to($1, link_url, link_options) + $'
  end

  def format_posting_rule_amount_or_percentage(entry)
    case entry[:rule]
    when "amount"     ; amount(entry[:number])
    when "percentage" ; number_to_percentage(100.0 * entry[:number], precision: 0)
    else
      raise "ASSERTION ERROR: expected one of amount or percentage for posting rule entry :rule value, found #{entry[:rule].inspect}"
    end
  end
end
