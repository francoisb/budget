# Budget

This is a small application to manage your home budget, based on patterns found in
[Analysis Patterns](http://www.amazon.com/Analysis-Patterns-Reusable-Object-Models/dp/0201895420?tag=duckduckgo-ffab-20).
