SET client_min_messages TO 'warning';

BEGIN;
SELECT plan(4);

-- Check primary key violations
INSERT INTO accounts(account, account_kind) VALUES ('compte maison', 'asset');
SELECT throws_ok('INSERT INTO accounts(account, account_kind) VALUES (''compte maison'', ''asset'')', 23505, null, 'primary key');

-- Check description length, trimness
SELECT throws_ok('INSERT INTO accounts(account, account_kind) VALUES ('' compte maison'', ''asset'')', 23514, null, 'account must be trimmed before');
SELECT throws_ok('INSERT INTO accounts(account, account_kind) VALUES (''compte maison '', ''asset'')', 23514, null, 'account must be trimmed after');
SELECT throws_ok('INSERT INTO accounts(account, account_kind) VALUES ('''', ''asset'')',               23514, null, 'account must be present');

-- Finish the tests and clean up.
SELECT * FROM finish();
ROLLBACK;
