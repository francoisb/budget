SET client_min_messages TO 'warning';

BEGIN;
SELECT plan(5);

-- Setup
INSERT INTO accounts(account, account_kind) VALUES
    ('bank', 'asset')
  , ('bank 2', 'asset')
  , ('credit card', 'liability')
  , ('salary', 'revenue')
  , ('electricity', 'expense')
  , ('mortgage', 'expense')
  , ('cell phone service', 'expense');

INSERT INTO memo_accounts(memo_account) VALUES
  ('mortgage'), ('electricity'), ('other'), ('earmarked');

-- Assert that we can receive a salary and earmark money for later
INSERT INTO transactions(transaction_id, posted_on, booked_on) VALUES ('7eabbdaa-0425-48a2-8304-e83614085045', '2015-08-18', '2015-08-22');
INSERT INTO transaction_entries(transaction_id, account, amount_dt, amount_ct) VALUES
    ('7eabbdaa-0425-48a2-8304-e83614085045', 'bank', '200.00', '0.00')
  , ('7eabbdaa-0425-48a2-8304-e83614085045', 'salary', '0.00', '200.00');

PREPARE write_01 AS
  INSERT INTO transaction_memos(transaction_id, account, memo_account, amount_dt, amount_ct) VALUES
      ('7eabbdaa-0425-48a2-8304-e83614085045', 'bank', 'mortgage',    '120.00', 0)
    , ('7eabbdaa-0425-48a2-8304-e83614085045', 'bank', 'electricity', '60.00', 0)
    , ('7eabbdaa-0425-48a2-8304-e83614085045', 'bank', 'other',       '20.00', 0)
;
SELECT lives_ok('write_01', 'write balanced entry');

-- Assert that we cannot earmark more money that we received
INSERT INTO transactions(transaction_id, posted_on, booked_on) VALUES ('db434be6-63c8-4184-bd8a-5a4ec9e93b03', '2015-08-18', '2015-08-22');
INSERT INTO transaction_entries(transaction_id, account, amount_dt, amount_ct) VALUES
    ('db434be6-63c8-4184-bd8a-5a4ec9e93b03', 'bank', '200.00', '0.00')
  , ('db434be6-63c8-4184-bd8a-5a4ec9e93b03', 'salary', '0.00', '200.00');

PREPARE write_02 AS
  INSERT INTO transaction_memos(transaction_id, account, memo_account, amount_dt, amount_ct) VALUES
      ('db434be6-63c8-4184-bd8a-5a4ec9e93b03', 'bank', 'mortgage',    '200.00', 0)
    , ('db434be6-63c8-4184-bd8a-5a4ec9e93b03', 'bank', 'electricity', '60.00', 0)
    , ('db434be6-63c8-4184-bd8a-5a4ec9e93b03', 'bank', 'other',       '20.00', 0)
;
SELECT throws_ok('write_02', 'P0001', 'Transaction db434be6-63c8-4184-bd8a-5a4ec9e93b03 is unbalanced in account bank: memo''s sum is (280.00 - 0) = 280.00 while entry''s sum is (200.00 - 0.00) = 200.00');

-- Assert that we can transfer money between accounts / memo accounts without issues
INSERT INTO transactions(transaction_id, posted_on, booked_on) VALUES ('9d38138e-d377-41a7-8ab1-d702e50d1387', '2015-08-19', '2015-08-24');
INSERT INTO transaction_entries(transaction_id, account, amount_dt, amount_ct) VALUES
    ('9d38138e-d377-41a7-8ab1-d702e50d1387', 'bank', 183, 0)
  , ('9d38138e-d377-41a7-8ab1-d702e50d1387', 'salary', '0', 183);

INSERT INTO transactions(transaction_id, posted_on, booked_on) VALUES ('ac521b64-5c17-4a1a-9540-7c972915f7e2', '2015-08-24', '2015-08-24');
INSERT INTO transaction_entries(transaction_id, account, amount_dt, amount_ct) VALUES
    ('ac521b64-5c17-4a1a-9540-7c972915f7e2', 'bank 2', 100, 0)
  , ('ac521b64-5c17-4a1a-9540-7c972915f7e2', 'bank', 0, 100);

PREPARE write_03 AS
  INSERT INTO transaction_memos(transaction_id, account, memo_account, amount_dt, amount_ct) VALUES
      ('ac521b64-5c17-4a1a-9540-7c972915f7e2', 'bank 2', 'mortgage', 80, 0)
    , ('ac521b64-5c17-4a1a-9540-7c972915f7e2', 'bank 2', 'electricity', 20, 0)
;
SELECT lives_ok('write_03', 'accepts sum of memos as 0, meaning transaction is well-balanced');

-- Assert that we can pay a bunch of things from memos in one shot

INSERT INTO transactions(transaction_id, posted_on, booked_on) VALUES ('d74ef775-4516-4306-89c5-e69e857337e9', '2015-08-24', '2015-08-24');
INSERT INTO transaction_entries(transaction_id, account, amount_dt, amount_ct) VALUES
    ('d74ef775-4516-4306-89c5-e69e857337e9', 'electricity', 20, 0)
  , ('d74ef775-4516-4306-89c5-e69e857337e9', 'mortgage', 80, 0)
  , ('d74ef775-4516-4306-89c5-e69e857337e9', 'bank 2', 0, 100);

PREPARE write_04 AS
  INSERT INTO transaction_memos(transaction_id, account, memo_account, amount_dt, amount_ct) VALUES
      ('d74ef775-4516-4306-89c5-e69e857337e9', 'bank 2', 'mortgage', 0, 80)
    , ('d74ef775-4516-4306-89c5-e69e857337e9', 'bank 2', 'electricity', 0, 20)
;
SELECT lives_ok('write_04', 'accepts abs(sum of memos) = transaction amount (all payments, no accumulation of money)');

-- Assert that we can only use asset accounts to fund memo transactions
INSERT INTO transactions(transaction_id, posted_on, booked_on) VALUES ('722bcfe4-16c1-4fe0-afd2-01657a465178', '2015-09-07', '2015-09-07');
INSERT INTO transaction_entries(transaction_id, account, amount_dt, amount_ct) VALUES
    ('722bcfe4-16c1-4fe0-afd2-01657a465178', 'mortgage', '100', '0')
  , ('722bcfe4-16c1-4fe0-afd2-01657a465178', 'credit card', '0', '100');

PREPARE write_05 AS
  INSERT INTO transaction_memos(transaction_id, account, memo_account, amount_dt, amount_ct) VALUES
      ('722bcfe4-16c1-4fe0-afd2-01657a465178', 'credit card', 'mortgage', '0', '100');
;
SELECT throws_ok('write_05', 'P0001', 'Transaction 722bcfe4-16c1-4fe0-afd2-01657a465178 is invalid: referenced account (credit card) from [mortgage] memo account must be an [asset] account, not a [liability]', 'must use an asset account to fund transaction_memos');

SELECT * FROM finish();
ROLLBACK;
