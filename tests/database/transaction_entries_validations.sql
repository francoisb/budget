SET client_min_messages TO 'warning';

BEGIN;
SELECT plan(6);

-- Setup
INSERT INTO accounts(account, account_kind) VALUES
    ('bank', 'asset')
  , ('credit card', 'liability')
  , ('salary', 'revenue')
  , ('cell phone service', 'expense');

-- Assert that we can write a balanced entry no problem
INSERT INTO transactions(transaction_id, posted_on, booked_on) VALUES ('a4999dd1-1356-4608-9333-0f392762339e', '2015-08-18', '2015-08-22');
PREPARE write_entries_01 AS
  INSERT INTO transaction_entries(transaction_id, account, amount_dt, amount_ct) VALUES
    ('a4999dd1-1356-4608-9333-0f392762339e', 'bank', '0.00', '74.59')
  , ('a4999dd1-1356-4608-9333-0f392762339e', 'cell phone service', '74.59', '0.00')
;
SELECT lives_ok('write_entries_01', 'write valid entries');

-- Assert that deleting rows will trigger the unbalanced check
SELECT throws_ok('DELETE FROM transaction_entries WHERE transaction_id = ''a4999dd1-1356-4608-9333-0f392762339e'' AND account = ''cell phone service''', null, 'Transaction a4999dd1-1356-4608-9333-0f392762339e is unbalanced: debits are 0.00 while credits are 74.59', 'delete transaction entry triggers unbalanced check');

-- Assert that updating rows will trigger the unbalanced check
SELECT throws_ok('UPDATE transaction_entries SET amount_dt = ''75.00'' WHERE transaction_id = ''a4999dd1-1356-4608-9333-0f392762339e'' AND account = ''cell phone service''', null, 'Transaction a4999dd1-1356-4608-9333-0f392762339e is unbalanced: debits are 75.00 while credits are 74.59', 'update transaction entry triggers unbalanced check');


-- Assert that using unbalanced amounts fails
INSERT INTO transactions(transaction_id, posted_on, booked_on) VALUES ('8a0ca8a3-6d17-47ff-a743-2ad2cc221f20', '2015-08-18', '2015-08-22');
PREPARE write_entries_02 AS
  INSERT INTO transaction_entries(transaction_id, account, amount_dt, amount_ct) VALUES
    ('8a0ca8a3-6d17-47ff-a743-2ad2cc221f20', 'salary', '0.00', '1950.00')
  , ('8a0ca8a3-6d17-47ff-a743-2ad2cc221f20', 'bank', '2000.00', '0.00')
;
SELECT throws_ok('write_entries_02', null, 'Transaction 8a0ca8a3-6d17-47ff-a743-2ad2cc221f20 is unbalanced: debits are 2000.00 while credits are 1950.00', 'write unbalanced entries with two accounts');

PREPARE write_entries_03 AS
  INSERT INTO transaction_entries(transaction_id, account, amount_dt, amount_ct) VALUES
    ('8a0ca8a3-6d17-47ff-a743-2ad2cc221f20', 'salary', '0.00', '1950.00')
  , ('8a0ca8a3-6d17-47ff-a743-2ad2cc221f20', 'cell phone service', '145.0', '0.00')
  , ('8a0ca8a3-6d17-47ff-a743-2ad2cc221f20', 'credit card', '458.28', '0.00')
  , ('8a0ca8a3-6d17-47ff-a743-2ad2cc221f20', 'bank', '1346.73', '0.00')
;
SELECT throws_ok('write_entries_03', null, 'Transaction 8a0ca8a3-6d17-47ff-a743-2ad2cc221f20 is unbalanced: debits are 1950.01 while credits are 1950.00', 'write unbalanced entries with multiple accounts');


-- Assert that we cannot use the same account twice in the same transaction
PREPARE write_entries_04 AS
  INSERT INTO transaction_entries(transaction_id, account, amount_dt, amount_ct) VALUES
    ('8a0ca8a3-6d17-47ff-a743-2ad2cc221f20', 'salary', '0.00', '2000.00')
  , ('8a0ca8a3-6d17-47ff-a743-2ad2cc221f20', 'salary', '2000.00', '0.00')
;
SELECT throws_ok('write_entries_04', '23505', null, 'cannot use the same account twice in a single transaction');

SELECT * FROM finish();
ROLLBACK;
