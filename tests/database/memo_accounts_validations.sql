SET client_min_messages TO 'warning';

BEGIN;
SELECT plan(4);

-- Check primary key violations
INSERT INTO memo_accounts(memo_account) VALUES ('cell service');
SELECT throws_ok('INSERT INTO memo_accounts(memo_account) VALUES (''cell service'')', 23505, null, 'primary key');

-- Check description length, trimness
SELECT throws_ok('INSERT INTO memo_accounts(memo_account) VALUES ('' cable service'')', 23514, null, 'memo_account must be trimmed before');
SELECT throws_ok('INSERT INTO memo_accounts(memo_account) VALUES (''cable service '')', 23514, null, 'memo_account must be trimmed after');
SELECT throws_ok('INSERT INTO memo_accounts(memo_account) VALUES ('''')',               23514, null, 'memo_account must be present');

-- Finish the tests and clean up.
SELECT * FROM finish();
ROLLBACK;
